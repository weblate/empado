// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/enums/emotion_category.dart';
import 'package:empado/enums/emotion_intensity.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum Emotion {
  lively(EmotionCategory.excited, EmotionIntensity.lowest),
  interested(EmotionCategory.excited, EmotionIntensity.lower),
  focused(EmotionCategory.excited, EmotionIntensity.low),
  inspired(EmotionCategory.excited, EmotionIntensity.medium),
  enthusiastic(EmotionCategory.excited, EmotionIntensity.high),
  excited(EmotionCategory.excited, EmotionIntensity.higher),
  exhilirated(EmotionCategory.excited, EmotionIntensity.highest),
  relaxed(EmotionCategory.content, EmotionIntensity.lowest),
  calm(EmotionCategory.content, EmotionIntensity.lower),
  pleasant(EmotionCategory.content, EmotionIntensity.low),
  content(EmotionCategory.content, EmotionIntensity.medium),
  joyful(EmotionCategory.content, EmotionIntensity.high),
  happy(EmotionCategory.content, EmotionIntensity.higher),
  blissful(EmotionCategory.content, EmotionIntensity.highest),
  bored(EmotionCategory.disgusted, EmotionIntensity.lowest),
  apathetic(EmotionCategory.disgusted, EmotionIntensity.lower),
  bitter(EmotionCategory.disgusted, EmotionIntensity.low),
  repulsed(EmotionCategory.disgusted, EmotionIntensity.medium),
  disgusted(EmotionCategory.disgusted, EmotionIntensity.high),
  contemptuous(EmotionCategory.disgusted, EmotionIntensity.higher),
  loathful(EmotionCategory.disgusted, EmotionIntensity.highest),
  unfocused(EmotionCategory.surprised, EmotionIntensity.lowest),
  distracted(EmotionCategory.surprised, EmotionIntensity.lower),
  surprised(EmotionCategory.surprised, EmotionIntensity.low),
  amazed(EmotionCategory.surprised, EmotionIntensity.medium),
  aghast(EmotionCategory.surprised, EmotionIntensity.high),
  shocked(EmotionCategory.surprised, EmotionIntensity.higher),
  awestruck(EmotionCategory.surprised, EmotionIntensity.highest),
  vulnerable(EmotionCategory.ashamed, EmotionIntensity.lowest),
  flustered(EmotionCategory.ashamed, EmotionIntensity.lower),
  embarassed(EmotionCategory.ashamed, EmotionIntensity.low),
  inferior(EmotionCategory.ashamed, EmotionIntensity.medium),
  regretful(EmotionCategory.ashamed, EmotionIntensity.high),
  guiltyAshamed(EmotionCategory.ashamed, EmotionIntensity.higher),
  deeplyGuiltyHumiliated(EmotionCategory.ashamed, EmotionIntensity.highest),
  pensive(EmotionCategory.sad, EmotionIntensity.lowest),
  down(EmotionCategory.sad, EmotionIntensity.lower),
  sad(EmotionCategory.sad, EmotionIntensity.low),
  mournful(EmotionCategory.sad, EmotionIntensity.medium),
  sorrowful(EmotionCategory.sad, EmotionIntensity.high),
  miserable(EmotionCategory.sad, EmotionIntensity.higher),
  anguished(EmotionCategory.sad, EmotionIntensity.highest),
  peeved(EmotionCategory.angry, EmotionIntensity.lowest),
  irritated(EmotionCategory.angry, EmotionIntensity.lower),
  upset(EmotionCategory.angry, EmotionIntensity.low),
  angry(EmotionCategory.angry, EmotionIntensity.medium),
  furious(EmotionCategory.angry, EmotionIntensity.high),
  livid(EmotionCategory.angry, EmotionIntensity.higher),
  enraged(EmotionCategory.angry, EmotionIntensity.highest),
  jittery(EmotionCategory.scared, EmotionIntensity.lowest),
  concerned(EmotionCategory.scared, EmotionIntensity.lower),
  nervous(EmotionCategory.scared, EmotionIntensity.low),
  worried(EmotionCategory.scared, EmotionIntensity.medium),
  scared(EmotionCategory.scared, EmotionIntensity.high),
  terrified(EmotionCategory.scared, EmotionIntensity.higher),
  panicked(EmotionCategory.scared, EmotionIntensity.highest);

  final EmotionCategory category;
  final EmotionIntensity intensity;

  const Emotion(this.category, this.intensity);

  static Emotion fromCategoryAndIntensity(
      EmotionCategory category, EmotionIntensity intensity) {
    switch (category) {
      case EmotionCategory.excited:
        switch (intensity) {
          case EmotionIntensity.lowest:
            return Emotion.lively;
          case EmotionIntensity.lower:
            return Emotion.interested;
          case EmotionIntensity.low:
            return Emotion.focused;
          case EmotionIntensity.medium:
            return Emotion.inspired;
          case EmotionIntensity.high:
            return Emotion.enthusiastic;
          case EmotionIntensity.higher:
            return Emotion.excited;
          case EmotionIntensity.highest:
            return Emotion.exhilirated;
        }
      case EmotionCategory.content:
        switch (intensity) {
          case EmotionIntensity.lowest:
            return Emotion.relaxed;
          case EmotionIntensity.lower:
            return Emotion.calm;
          case EmotionIntensity.low:
            return Emotion.pleasant;
          case EmotionIntensity.medium:
            return Emotion.content;
          case EmotionIntensity.high:
            return Emotion.joyful;
          case EmotionIntensity.higher:
            return Emotion.happy;
          case EmotionIntensity.highest:
            return Emotion.blissful;
        }
      case EmotionCategory.disgusted:
        switch (intensity) {
          case EmotionIntensity.lowest:
            return Emotion.bored;
          case EmotionIntensity.lower:
            return Emotion.apathetic;
          case EmotionIntensity.low:
            return Emotion.bitter;
          case EmotionIntensity.medium:
            return Emotion.repulsed;
          case EmotionIntensity.high:
            return Emotion.disgusted;
          case EmotionIntensity.higher:
            return Emotion.contemptuous;
          case EmotionIntensity.highest:
            return Emotion.loathful;
        }
      case EmotionCategory.surprised:
        switch (intensity) {
          case EmotionIntensity.lowest:
            return Emotion.unfocused;
          case EmotionIntensity.lower:
            return Emotion.distracted;
          case EmotionIntensity.low:
            return Emotion.surprised;
          case EmotionIntensity.medium:
            return Emotion.amazed;
          case EmotionIntensity.high:
            return Emotion.aghast;
          case EmotionIntensity.higher:
            return Emotion.shocked;
          case EmotionIntensity.highest:
            return Emotion.awestruck;
        }
      case EmotionCategory.ashamed:
        switch (intensity) {
          case EmotionIntensity.lowest:
            return Emotion.vulnerable;
          case EmotionIntensity.lower:
            return Emotion.flustered;
          case EmotionIntensity.low:
            return Emotion.embarassed;
          case EmotionIntensity.medium:
            return Emotion.inferior;
          case EmotionIntensity.high:
            return Emotion.regretful;
          case EmotionIntensity.higher:
            return Emotion.guiltyAshamed;
          case EmotionIntensity.highest:
            return Emotion.deeplyGuiltyHumiliated;
        }
      case EmotionCategory.sad:
        switch (intensity) {
          case EmotionIntensity.lowest:
            return Emotion.pensive;
          case EmotionIntensity.lower:
            return Emotion.down;
          case EmotionIntensity.low:
            return Emotion.sad;
          case EmotionIntensity.medium:
            return Emotion.mournful;
          case EmotionIntensity.high:
            return Emotion.sorrowful;
          case EmotionIntensity.higher:
            return Emotion.miserable;
          case EmotionIntensity.highest:
            return Emotion.anguished;
        }
      case EmotionCategory.angry:
        switch (intensity) {
          case EmotionIntensity.lowest:
            return Emotion.peeved;
          case EmotionIntensity.lower:
            return Emotion.irritated;
          case EmotionIntensity.low:
            return Emotion.upset;
          case EmotionIntensity.medium:
            return Emotion.angry;
          case EmotionIntensity.high:
            return Emotion.furious;
          case EmotionIntensity.higher:
            return Emotion.livid;
          case EmotionIntensity.highest:
            return Emotion.enraged;
        }
      case EmotionCategory.scared:
        switch (intensity) {
          case EmotionIntensity.lowest:
            return Emotion.jittery;
          case EmotionIntensity.lower:
            return Emotion.concerned;
          case EmotionIntensity.low:
            return Emotion.nervous;
          case EmotionIntensity.medium:
            return Emotion.worried;
          case EmotionIntensity.high:
            return Emotion.scared;
          case EmotionIntensity.higher:
            return Emotion.terrified;
          case EmotionIntensity.highest:
            return Emotion.panicked;
        }
    }
  }

  String getTranslation(BuildContext context) {
    switch (this) {
      case Emotion.lively:
        return AppLocalizations.of(context).emotionLively;
      case Emotion.interested:
        return AppLocalizations.of(context).emotionInterested;
      case Emotion.focused:
        return AppLocalizations.of(context).emotionFocused;
      case Emotion.inspired:
        return AppLocalizations.of(context).emotionInspired;
      case Emotion.enthusiastic:
        return AppLocalizations.of(context).emotionEnthusiastic;
      case Emotion.excited:
        return AppLocalizations.of(context).emotionExcited;
      case Emotion.exhilirated:
        return AppLocalizations.of(context).emotionExhilirated;
      case Emotion.relaxed:
        return AppLocalizations.of(context).emotionRelaxed;
      case Emotion.calm:
        return AppLocalizations.of(context).emotionCalm;
      case Emotion.pleasant:
        return AppLocalizations.of(context).emotionPleasant;
      case Emotion.content:
        return AppLocalizations.of(context).emotionContent;
      case Emotion.joyful:
        return AppLocalizations.of(context).emotionJoyful;
      case Emotion.happy:
        return AppLocalizations.of(context).emotionHappy;
      case Emotion.blissful:
        return AppLocalizations.of(context).emotionBlissful;
      case Emotion.bored:
        return AppLocalizations.of(context).emotionBored;
      case Emotion.apathetic:
        return AppLocalizations.of(context).emotionApathetic;
      case Emotion.repulsed:
        return AppLocalizations.of(context).emotionRepulsed;
      case Emotion.bitter:
        return AppLocalizations.of(context).emotionBitter;
      case Emotion.disgusted:
        return AppLocalizations.of(context).emotionDisgusted;
      case Emotion.contemptuous:
        return AppLocalizations.of(context).emotionContemptuous;
      case Emotion.loathful:
        return AppLocalizations.of(context).emotionLoathful;
      case Emotion.unfocused:
        return AppLocalizations.of(context).emotionUnfocused;
      case Emotion.distracted:
        return AppLocalizations.of(context).emotionDistracted;
      case Emotion.surprised:
        return AppLocalizations.of(context).emotionSurprised;
      case Emotion.amazed:
        return AppLocalizations.of(context).emotionAmazed;
      case Emotion.aghast:
        return AppLocalizations.of(context).emotionAghast;
      case Emotion.shocked:
        return AppLocalizations.of(context).emotionShocked;
      case Emotion.awestruck:
        return AppLocalizations.of(context).emotionAwestruck;
      case Emotion.vulnerable:
        return AppLocalizations.of(context).emotionVulnerable;
      case Emotion.flustered:
        return AppLocalizations.of(context).emotionFlustered;
      case Emotion.embarassed:
        return AppLocalizations.of(context).emotionEmbarassed;
      case Emotion.regretful:
        return AppLocalizations.of(context).emotionRegretful;
      case Emotion.inferior:
        return AppLocalizations.of(context).emotionInferior;
      case Emotion.guiltyAshamed:
        return AppLocalizations.of(context).emotionGuiltyAshamed;
      case Emotion.deeplyGuiltyHumiliated:
        return AppLocalizations.of(context).emotionDeeplyGuiltyHumiliated;
      case Emotion.pensive:
        return AppLocalizations.of(context).emotionPensive;
      case Emotion.down:
        return AppLocalizations.of(context).emotionDown;
      case Emotion.sad:
        return AppLocalizations.of(context).emotionSad;
      case Emotion.mournful:
        return AppLocalizations.of(context).emotionMournful;
      case Emotion.sorrowful:
        return AppLocalizations.of(context).emotionSorrowful;
      case Emotion.miserable:
        return AppLocalizations.of(context).emotionMiserable;
      case Emotion.anguished:
        return AppLocalizations.of(context).emotionAnguished;
      case Emotion.irritated:
        return AppLocalizations.of(context).emotionIrritated;
      case Emotion.peeved:
        return AppLocalizations.of(context).emotionPeeved;
      case Emotion.upset:
        return AppLocalizations.of(context).emotionUpset;
      case Emotion.angry:
        return AppLocalizations.of(context).emotionAngry;
      case Emotion.furious:
        return AppLocalizations.of(context).emotionFurious;
      case Emotion.livid:
        return AppLocalizations.of(context).emotionLivid;
      case Emotion.enraged:
        return AppLocalizations.of(context).emotionEnraged;
      case Emotion.jittery:
        return AppLocalizations.of(context).emotionJittery;
      case Emotion.concerned:
        return AppLocalizations.of(context).emotionConcerned;
      case Emotion.nervous:
        return AppLocalizations.of(context).emotionNervous;
      case Emotion.scared:
        return AppLocalizations.of(context).emotionScared;
      case Emotion.worried:
        return AppLocalizations.of(context).emotionWorried;
      case Emotion.terrified:
        return AppLocalizations.of(context).emotionTerrified;
      case Emotion.panicked:
        return AppLocalizations.of(context).emotionPanicked;
    }
  }
}
