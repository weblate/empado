// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/enums/emotion_pleasantness.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum EmotionCategory {
  excited(IconData(0xe806, fontFamily: _iconFont), Color(0xffFFFFFF),
      Color(0xdd000000), EmotionPleasantness.pleasant),
  content(IconData(0xe804, fontFamily: _iconFont), Color(0xffF6D32D),
      Color(0xdd000000), EmotionPleasantness.pleasant),
  disgusted(IconData(0xe805, fontFamily: _iconFont), Color(0xff57E389),
      Color(0xdd000000), EmotionPleasantness.unpleasant),
  surprised(IconData(0xe807, fontFamily: _iconFont), Color(0xff33D1D1),
      Color(0xdd000000), EmotionPleasantness.neutral),
  ashamed(IconData(0xe803, fontFamily: _iconFont), Color(0xff838287),
      Color(0xFF000000), EmotionPleasantness.unpleasant),
  sad(IconData(0xe801, fontFamily: _iconFont), Color(0xff99C1F1),
      Color(0xdd000000), EmotionPleasantness.unpleasant),
  angry(IconData(0xe802, fontFamily: _iconFont), Color(0xffC061CB),
      Color(0xdd000000), EmotionPleasantness.unpleasant),
  scared(IconData(0xe800, fontFamily: _iconFont), Color(0xffF66151),
      Color(0xdd000000), EmotionPleasantness.unpleasant);

  static const _iconFont = "Emotion Icons";

  final IconData iconData;
  final Color backgroundColor;
  final Color foregroundColor;
  final EmotionPleasantness pleasantness;

  const EmotionCategory(this.iconData, this.backgroundColor,
      this.foregroundColor, this.pleasantness);

  String getTranslation(BuildContext context) {
    switch (this) {
      case EmotionCategory.excited:
        return AppLocalizations.of(context).emotionCategoryExcited;
      case EmotionCategory.content:
        return AppLocalizations.of(context).emotionCategoryContent;
      case EmotionCategory.disgusted:
        return AppLocalizations.of(context).emotionCategoryDisgusted;
      case EmotionCategory.surprised:
        return AppLocalizations.of(context).emotionCategorySurprised;
      case EmotionCategory.ashamed:
        return AppLocalizations.of(context).emotionCategoryAshamed;
      case EmotionCategory.sad:
        return AppLocalizations.of(context).emotionCategorySad;
      case EmotionCategory.angry:
        return AppLocalizations.of(context).emotionCategoryAngry;
      case EmotionCategory.scared:
        return AppLocalizations.of(context).emotionCategoryScared;
    }
  }
}
