// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// ignore_for_file: type=lint
class $SituationsTable extends Situations
    with TableInfo<$SituationsTable, Situation> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SituationsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _timestampMeta =
      const VerificationMeta('timestamp');
  @override
  late final GeneratedColumn<DateTime> timestamp = GeneratedColumn<DateTime>(
      'timestamp', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _titleMeta = const VerificationMeta('title');
  @override
  late final GeneratedColumn<String> title = GeneratedColumn<String>(
      'title', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [id, timestamp, title];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'situations';
  @override
  VerificationContext validateIntegrity(Insertable<Situation> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('timestamp')) {
      context.handle(_timestampMeta,
          timestamp.isAcceptableOrUnknown(data['timestamp']!, _timestampMeta));
    } else if (isInserting) {
      context.missing(_timestampMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Situation map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Situation(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      timestamp: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}timestamp'])!,
      title: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}title']),
    );
  }

  @override
  $SituationsTable createAlias(String alias) {
    return $SituationsTable(attachedDatabase, alias);
  }
}

class Situation extends DataClass implements Insertable<Situation> {
  final int id;
  final DateTime timestamp;
  final String? title;
  const Situation({required this.id, required this.timestamp, this.title});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['timestamp'] = Variable<DateTime>(timestamp);
    if (!nullToAbsent || title != null) {
      map['title'] = Variable<String>(title);
    }
    return map;
  }

  SituationsCompanion toCompanion(bool nullToAbsent) {
    return SituationsCompanion(
      id: Value(id),
      timestamp: Value(timestamp),
      title:
          title == null && nullToAbsent ? const Value.absent() : Value(title),
    );
  }

  factory Situation.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Situation(
      id: serializer.fromJson<int>(json['id']),
      timestamp: serializer.fromJson<DateTime>(json['timestamp']),
      title: serializer.fromJson<String?>(json['title']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'timestamp': serializer.toJson<DateTime>(timestamp),
      'title': serializer.toJson<String?>(title),
    };
  }

  Situation copyWith(
          {int? id,
          DateTime? timestamp,
          Value<String?> title = const Value.absent()}) =>
      Situation(
        id: id ?? this.id,
        timestamp: timestamp ?? this.timestamp,
        title: title.present ? title.value : this.title,
      );
  @override
  String toString() {
    return (StringBuffer('Situation(')
          ..write('id: $id, ')
          ..write('timestamp: $timestamp, ')
          ..write('title: $title')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, timestamp, title);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Situation &&
          other.id == this.id &&
          other.timestamp == this.timestamp &&
          other.title == this.title);
}

class SituationsCompanion extends UpdateCompanion<Situation> {
  final Value<int> id;
  final Value<DateTime> timestamp;
  final Value<String?> title;
  const SituationsCompanion({
    this.id = const Value.absent(),
    this.timestamp = const Value.absent(),
    this.title = const Value.absent(),
  });
  SituationsCompanion.insert({
    this.id = const Value.absent(),
    required DateTime timestamp,
    this.title = const Value.absent(),
  }) : timestamp = Value(timestamp);
  static Insertable<Situation> custom({
    Expression<int>? id,
    Expression<DateTime>? timestamp,
    Expression<String>? title,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (timestamp != null) 'timestamp': timestamp,
      if (title != null) 'title': title,
    });
  }

  SituationsCompanion copyWith(
      {Value<int>? id, Value<DateTime>? timestamp, Value<String?>? title}) {
    return SituationsCompanion(
      id: id ?? this.id,
      timestamp: timestamp ?? this.timestamp,
      title: title ?? this.title,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (timestamp.present) {
      map['timestamp'] = Variable<DateTime>(timestamp.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SituationsCompanion(')
          ..write('id: $id, ')
          ..write('timestamp: $timestamp, ')
          ..write('title: $title')
          ..write(')'))
        .toString();
  }
}

class $PeopleTable extends People with TableInfo<$PeopleTable, Person> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $PeopleTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _pronounMeta =
      const VerificationMeta('pronoun');
  @override
  late final GeneratedColumnWithTypeConverter<Pronoun, int> pronoun =
      GeneratedColumn<int>('pronoun', aliasedName, false,
              type: DriftSqlType.int, requiredDuringInsert: true)
          .withConverter<Pronoun>($PeopleTable.$converterpronoun);
  @override
  List<GeneratedColumn> get $columns => [id, name, pronoun];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'people';
  @override
  VerificationContext validateIntegrity(Insertable<Person> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    }
    context.handle(_pronounMeta, const VerificationResult.success());
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Person map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Person(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name']),
      pronoun: $PeopleTable.$converterpronoun.fromSql(attachedDatabase
          .typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}pronoun'])!),
    );
  }

  @override
  $PeopleTable createAlias(String alias) {
    return $PeopleTable(attachedDatabase, alias);
  }

  static JsonTypeConverter2<Pronoun, int, int> $converterpronoun =
      const EnumIndexConverter<Pronoun>(Pronoun.values);
}

class Person extends DataClass implements Insertable<Person> {
  final int id;
  final String? name;
  final Pronoun pronoun;
  const Person({required this.id, this.name, required this.pronoun});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    {
      map['pronoun'] =
          Variable<int>($PeopleTable.$converterpronoun.toSql(pronoun));
    }
    return map;
  }

  PeopleCompanion toCompanion(bool nullToAbsent) {
    return PeopleCompanion(
      id: Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      pronoun: Value(pronoun),
    );
  }

  factory Person.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Person(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String?>(json['name']),
      pronoun: $PeopleTable.$converterpronoun
          .fromJson(serializer.fromJson<int>(json['pronoun'])),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String?>(name),
      'pronoun': serializer
          .toJson<int>($PeopleTable.$converterpronoun.toJson(pronoun)),
    };
  }

  Person copyWith(
          {int? id,
          Value<String?> name = const Value.absent(),
          Pronoun? pronoun}) =>
      Person(
        id: id ?? this.id,
        name: name.present ? name.value : this.name,
        pronoun: pronoun ?? this.pronoun,
      );
  @override
  String toString() {
    return (StringBuffer('Person(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('pronoun: $pronoun')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, name, pronoun);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Person &&
          other.id == this.id &&
          other.name == this.name &&
          other.pronoun == this.pronoun);
}

class PeopleCompanion extends UpdateCompanion<Person> {
  final Value<int> id;
  final Value<String?> name;
  final Value<Pronoun> pronoun;
  const PeopleCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.pronoun = const Value.absent(),
  });
  PeopleCompanion.insert({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    required Pronoun pronoun,
  }) : pronoun = Value(pronoun);
  static Insertable<Person> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<int>? pronoun,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (pronoun != null) 'pronoun': pronoun,
    });
  }

  PeopleCompanion copyWith(
      {Value<int>? id, Value<String?>? name, Value<Pronoun>? pronoun}) {
    return PeopleCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      pronoun: pronoun ?? this.pronoun,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (pronoun.present) {
      map['pronoun'] =
          Variable<int>($PeopleTable.$converterpronoun.toSql(pronoun.value));
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PeopleCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('pronoun: $pronoun')
          ..write(')'))
        .toString();
  }
}

class $StrategiesTable extends Strategies
    with TableInfo<$StrategiesTable, Strategy> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $StrategiesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _strategyMeta =
      const VerificationMeta('strategy');
  @override
  late final GeneratedColumn<String> strategy = GeneratedColumn<String>(
      'strategy', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _archivedMeta =
      const VerificationMeta('archived');
  @override
  late final GeneratedColumn<bool> archived = GeneratedColumn<bool>(
      'archived', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("archived" IN (0, 1))'),
      defaultValue: const Constant(false));
  static const VerificationMeta _bookmarkedMeta =
      const VerificationMeta('bookmarked');
  @override
  late final GeneratedColumn<bool> bookmarked = GeneratedColumn<bool>(
      'bookmarked', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("bookmarked" IN (0, 1))'),
      defaultValue: const Constant(false));
  static const VerificationMeta _lastUsedMeta =
      const VerificationMeta('lastUsed');
  @override
  late final GeneratedColumn<DateTime> lastUsed = GeneratedColumn<DateTime>(
      'last_used', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, strategy, archived, bookmarked, lastUsed];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'strategies';
  @override
  VerificationContext validateIntegrity(Insertable<Strategy> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('strategy')) {
      context.handle(_strategyMeta,
          strategy.isAcceptableOrUnknown(data['strategy']!, _strategyMeta));
    } else if (isInserting) {
      context.missing(_strategyMeta);
    }
    if (data.containsKey('archived')) {
      context.handle(_archivedMeta,
          archived.isAcceptableOrUnknown(data['archived']!, _archivedMeta));
    }
    if (data.containsKey('bookmarked')) {
      context.handle(
          _bookmarkedMeta,
          bookmarked.isAcceptableOrUnknown(
              data['bookmarked']!, _bookmarkedMeta));
    }
    if (data.containsKey('last_used')) {
      context.handle(_lastUsedMeta,
          lastUsed.isAcceptableOrUnknown(data['last_used']!, _lastUsedMeta));
    } else if (isInserting) {
      context.missing(_lastUsedMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Strategy map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Strategy(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      strategy: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}strategy'])!,
      archived: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}archived'])!,
      bookmarked: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}bookmarked'])!,
      lastUsed: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}last_used'])!,
    );
  }

  @override
  $StrategiesTable createAlias(String alias) {
    return $StrategiesTable(attachedDatabase, alias);
  }
}

class Strategy extends DataClass implements Insertable<Strategy> {
  final int id;
  final String strategy;
  final bool archived;
  final bool bookmarked;
  final DateTime lastUsed;
  const Strategy(
      {required this.id,
      required this.strategy,
      required this.archived,
      required this.bookmarked,
      required this.lastUsed});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['strategy'] = Variable<String>(strategy);
    map['archived'] = Variable<bool>(archived);
    map['bookmarked'] = Variable<bool>(bookmarked);
    map['last_used'] = Variable<DateTime>(lastUsed);
    return map;
  }

  StrategiesCompanion toCompanion(bool nullToAbsent) {
    return StrategiesCompanion(
      id: Value(id),
      strategy: Value(strategy),
      archived: Value(archived),
      bookmarked: Value(bookmarked),
      lastUsed: Value(lastUsed),
    );
  }

  factory Strategy.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Strategy(
      id: serializer.fromJson<int>(json['id']),
      strategy: serializer.fromJson<String>(json['strategy']),
      archived: serializer.fromJson<bool>(json['archived']),
      bookmarked: serializer.fromJson<bool>(json['bookmarked']),
      lastUsed: serializer.fromJson<DateTime>(json['lastUsed']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'strategy': serializer.toJson<String>(strategy),
      'archived': serializer.toJson<bool>(archived),
      'bookmarked': serializer.toJson<bool>(bookmarked),
      'lastUsed': serializer.toJson<DateTime>(lastUsed),
    };
  }

  Strategy copyWith(
          {int? id,
          String? strategy,
          bool? archived,
          bool? bookmarked,
          DateTime? lastUsed}) =>
      Strategy(
        id: id ?? this.id,
        strategy: strategy ?? this.strategy,
        archived: archived ?? this.archived,
        bookmarked: bookmarked ?? this.bookmarked,
        lastUsed: lastUsed ?? this.lastUsed,
      );
  @override
  String toString() {
    return (StringBuffer('Strategy(')
          ..write('id: $id, ')
          ..write('strategy: $strategy, ')
          ..write('archived: $archived, ')
          ..write('bookmarked: $bookmarked, ')
          ..write('lastUsed: $lastUsed')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, strategy, archived, bookmarked, lastUsed);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Strategy &&
          other.id == this.id &&
          other.strategy == this.strategy &&
          other.archived == this.archived &&
          other.bookmarked == this.bookmarked &&
          other.lastUsed == this.lastUsed);
}

class StrategiesCompanion extends UpdateCompanion<Strategy> {
  final Value<int> id;
  final Value<String> strategy;
  final Value<bool> archived;
  final Value<bool> bookmarked;
  final Value<DateTime> lastUsed;
  const StrategiesCompanion({
    this.id = const Value.absent(),
    this.strategy = const Value.absent(),
    this.archived = const Value.absent(),
    this.bookmarked = const Value.absent(),
    this.lastUsed = const Value.absent(),
  });
  StrategiesCompanion.insert({
    this.id = const Value.absent(),
    required String strategy,
    this.archived = const Value.absent(),
    this.bookmarked = const Value.absent(),
    required DateTime lastUsed,
  })  : strategy = Value(strategy),
        lastUsed = Value(lastUsed);
  static Insertable<Strategy> custom({
    Expression<int>? id,
    Expression<String>? strategy,
    Expression<bool>? archived,
    Expression<bool>? bookmarked,
    Expression<DateTime>? lastUsed,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (strategy != null) 'strategy': strategy,
      if (archived != null) 'archived': archived,
      if (bookmarked != null) 'bookmarked': bookmarked,
      if (lastUsed != null) 'last_used': lastUsed,
    });
  }

  StrategiesCompanion copyWith(
      {Value<int>? id,
      Value<String>? strategy,
      Value<bool>? archived,
      Value<bool>? bookmarked,
      Value<DateTime>? lastUsed}) {
    return StrategiesCompanion(
      id: id ?? this.id,
      strategy: strategy ?? this.strategy,
      archived: archived ?? this.archived,
      bookmarked: bookmarked ?? this.bookmarked,
      lastUsed: lastUsed ?? this.lastUsed,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (strategy.present) {
      map['strategy'] = Variable<String>(strategy.value);
    }
    if (archived.present) {
      map['archived'] = Variable<bool>(archived.value);
    }
    if (bookmarked.present) {
      map['bookmarked'] = Variable<bool>(bookmarked.value);
    }
    if (lastUsed.present) {
      map['last_used'] = Variable<DateTime>(lastUsed.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('StrategiesCompanion(')
          ..write('id: $id, ')
          ..write('strategy: $strategy, ')
          ..write('archived: $archived, ')
          ..write('bookmarked: $bookmarked, ')
          ..write('lastUsed: $lastUsed')
          ..write(')'))
        .toString();
  }
}

class $StrategyNeedsTable extends StrategyNeeds
    with TableInfo<$StrategyNeedsTable, StrategyNeed> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $StrategyNeedsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _strategyMeta =
      const VerificationMeta('strategy');
  @override
  late final GeneratedColumn<int> strategy = GeneratedColumn<int>(
      'strategy', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES strategies (id)'));
  static const VerificationMeta _needMeta = const VerificationMeta('need');
  @override
  late final GeneratedColumnWithTypeConverter<Need, String> need =
      GeneratedColumn<String>('need', aliasedName, false,
              type: DriftSqlType.string, requiredDuringInsert: true)
          .withConverter<Need>($StrategyNeedsTable.$converterneed);
  static const VerificationMeta _cachedNeedCategoryMeta =
      const VerificationMeta('cachedNeedCategory');
  @override
  late final GeneratedColumnWithTypeConverter<NeedCategory, String>
      cachedNeedCategory = GeneratedColumn<String>(
              'cached_need_category', aliasedName, false,
              type: DriftSqlType.string, requiredDuringInsert: true)
          .withConverter<NeedCategory>(
              $StrategyNeedsTable.$convertercachedNeedCategory);
  @override
  List<GeneratedColumn> get $columns => [strategy, need, cachedNeedCategory];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'strategy_needs';
  @override
  VerificationContext validateIntegrity(Insertable<StrategyNeed> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('strategy')) {
      context.handle(_strategyMeta,
          strategy.isAcceptableOrUnknown(data['strategy']!, _strategyMeta));
    } else if (isInserting) {
      context.missing(_strategyMeta);
    }
    context.handle(_needMeta, const VerificationResult.success());
    context.handle(_cachedNeedCategoryMeta, const VerificationResult.success());
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {strategy, need};
  @override
  StrategyNeed map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return StrategyNeed(
      strategy: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}strategy'])!,
      need: $StrategyNeedsTable.$converterneed.fromSql(attachedDatabase
          .typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}need'])!),
      cachedNeedCategory: $StrategyNeedsTable.$convertercachedNeedCategory
          .fromSql(attachedDatabase.typeMapping.read(DriftSqlType.string,
              data['${effectivePrefix}cached_need_category'])!),
    );
  }

  @override
  $StrategyNeedsTable createAlias(String alias) {
    return $StrategyNeedsTable(attachedDatabase, alias);
  }

  static JsonTypeConverter2<Need, String, String> $converterneed =
      const EnumNameConverter<Need>(Need.values);
  static JsonTypeConverter2<NeedCategory, String, String>
      $convertercachedNeedCategory =
      const EnumNameConverter<NeedCategory>(NeedCategory.values);
}

class StrategyNeed extends DataClass implements Insertable<StrategyNeed> {
  final int strategy;
  final Need need;
  final NeedCategory cachedNeedCategory;
  const StrategyNeed(
      {required this.strategy,
      required this.need,
      required this.cachedNeedCategory});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['strategy'] = Variable<int>(strategy);
    {
      map['need'] =
          Variable<String>($StrategyNeedsTable.$converterneed.toSql(need));
    }
    {
      map['cached_need_category'] = Variable<String>($StrategyNeedsTable
          .$convertercachedNeedCategory
          .toSql(cachedNeedCategory));
    }
    return map;
  }

  StrategyNeedsCompanion toCompanion(bool nullToAbsent) {
    return StrategyNeedsCompanion(
      strategy: Value(strategy),
      need: Value(need),
      cachedNeedCategory: Value(cachedNeedCategory),
    );
  }

  factory StrategyNeed.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return StrategyNeed(
      strategy: serializer.fromJson<int>(json['strategy']),
      need: $StrategyNeedsTable.$converterneed
          .fromJson(serializer.fromJson<String>(json['need'])),
      cachedNeedCategory: $StrategyNeedsTable.$convertercachedNeedCategory
          .fromJson(serializer.fromJson<String>(json['cachedNeedCategory'])),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'strategy': serializer.toJson<int>(strategy),
      'need': serializer
          .toJson<String>($StrategyNeedsTable.$converterneed.toJson(need)),
      'cachedNeedCategory': serializer.toJson<String>($StrategyNeedsTable
          .$convertercachedNeedCategory
          .toJson(cachedNeedCategory)),
    };
  }

  StrategyNeed copyWith(
          {int? strategy, Need? need, NeedCategory? cachedNeedCategory}) =>
      StrategyNeed(
        strategy: strategy ?? this.strategy,
        need: need ?? this.need,
        cachedNeedCategory: cachedNeedCategory ?? this.cachedNeedCategory,
      );
  @override
  String toString() {
    return (StringBuffer('StrategyNeed(')
          ..write('strategy: $strategy, ')
          ..write('need: $need, ')
          ..write('cachedNeedCategory: $cachedNeedCategory')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(strategy, need, cachedNeedCategory);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is StrategyNeed &&
          other.strategy == this.strategy &&
          other.need == this.need &&
          other.cachedNeedCategory == this.cachedNeedCategory);
}

class StrategyNeedsCompanion extends UpdateCompanion<StrategyNeed> {
  final Value<int> strategy;
  final Value<Need> need;
  final Value<NeedCategory> cachedNeedCategory;
  final Value<int> rowid;
  const StrategyNeedsCompanion({
    this.strategy = const Value.absent(),
    this.need = const Value.absent(),
    this.cachedNeedCategory = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  StrategyNeedsCompanion.insert({
    required int strategy,
    required Need need,
    required NeedCategory cachedNeedCategory,
    this.rowid = const Value.absent(),
  })  : strategy = Value(strategy),
        need = Value(need),
        cachedNeedCategory = Value(cachedNeedCategory);
  static Insertable<StrategyNeed> custom({
    Expression<int>? strategy,
    Expression<String>? need,
    Expression<String>? cachedNeedCategory,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (strategy != null) 'strategy': strategy,
      if (need != null) 'need': need,
      if (cachedNeedCategory != null)
        'cached_need_category': cachedNeedCategory,
      if (rowid != null) 'rowid': rowid,
    });
  }

  StrategyNeedsCompanion copyWith(
      {Value<int>? strategy,
      Value<Need>? need,
      Value<NeedCategory>? cachedNeedCategory,
      Value<int>? rowid}) {
    return StrategyNeedsCompanion(
      strategy: strategy ?? this.strategy,
      need: need ?? this.need,
      cachedNeedCategory: cachedNeedCategory ?? this.cachedNeedCategory,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (strategy.present) {
      map['strategy'] = Variable<int>(strategy.value);
    }
    if (need.present) {
      map['need'] = Variable<String>(
          $StrategyNeedsTable.$converterneed.toSql(need.value));
    }
    if (cachedNeedCategory.present) {
      map['cached_need_category'] = Variable<String>($StrategyNeedsTable
          .$convertercachedNeedCategory
          .toSql(cachedNeedCategory.value));
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('StrategyNeedsCompanion(')
          ..write('strategy: $strategy, ')
          ..write('need: $need, ')
          ..write('cachedNeedCategory: $cachedNeedCategory, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $PerspectivesTable extends Perspectives
    with TableInfo<$PerspectivesTable, Perspective> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $PerspectivesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _situationMeta =
      const VerificationMeta('situation');
  @override
  late final GeneratedColumn<int> situation = GeneratedColumn<int>(
      'situation', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES situations (id)'));
  static const VerificationMeta _personMeta = const VerificationMeta('person');
  @override
  late final GeneratedColumn<int> person = GeneratedColumn<int>(
      'person', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES people (id)'));
  static const VerificationMeta _observationMeta =
      const VerificationMeta('observation');
  @override
  late final GeneratedColumn<String> observation = GeneratedColumn<String>(
      'observation', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _additionalNotesMeta =
      const VerificationMeta('additionalNotes');
  @override
  late final GeneratedColumn<String> additionalNotes = GeneratedColumn<String>(
      'additional_notes', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _isAppreciationMeta =
      const VerificationMeta('isAppreciation');
  @override
  late final GeneratedColumn<bool> isAppreciation = GeneratedColumn<bool>(
      'is_appreciation', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("is_appreciation" IN (0, 1))'));
  static const VerificationMeta _ownDescriptionMeta =
      const VerificationMeta('ownDescription');
  @override
  late final GeneratedColumn<bool> ownDescription = GeneratedColumn<bool>(
      'own_description', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("own_description" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        situation,
        person,
        observation,
        additionalNotes,
        isAppreciation,
        ownDescription
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'perspectives';
  @override
  VerificationContext validateIntegrity(Insertable<Perspective> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('situation')) {
      context.handle(_situationMeta,
          situation.isAcceptableOrUnknown(data['situation']!, _situationMeta));
    } else if (isInserting) {
      context.missing(_situationMeta);
    }
    if (data.containsKey('person')) {
      context.handle(_personMeta,
          person.isAcceptableOrUnknown(data['person']!, _personMeta));
    } else if (isInserting) {
      context.missing(_personMeta);
    }
    if (data.containsKey('observation')) {
      context.handle(
          _observationMeta,
          observation.isAcceptableOrUnknown(
              data['observation']!, _observationMeta));
    }
    if (data.containsKey('additional_notes')) {
      context.handle(
          _additionalNotesMeta,
          additionalNotes.isAcceptableOrUnknown(
              data['additional_notes']!, _additionalNotesMeta));
    }
    if (data.containsKey('is_appreciation')) {
      context.handle(
          _isAppreciationMeta,
          isAppreciation.isAcceptableOrUnknown(
              data['is_appreciation']!, _isAppreciationMeta));
    } else if (isInserting) {
      context.missing(_isAppreciationMeta);
    }
    if (data.containsKey('own_description')) {
      context.handle(
          _ownDescriptionMeta,
          ownDescription.isAcceptableOrUnknown(
              data['own_description']!, _ownDescriptionMeta));
    } else if (isInserting) {
      context.missing(_ownDescriptionMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Perspective map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Perspective(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      situation: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}situation'])!,
      person: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}person'])!,
      observation: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}observation']),
      additionalNotes: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}additional_notes']),
      isAppreciation: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}is_appreciation'])!,
      ownDescription: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}own_description'])!,
    );
  }

  @override
  $PerspectivesTable createAlias(String alias) {
    return $PerspectivesTable(attachedDatabase, alias);
  }
}

class Perspective extends DataClass implements Insertable<Perspective> {
  final int id;
  final int situation;
  final int person;
  final String? observation;
  final String? additionalNotes;
  final bool isAppreciation;
  final bool ownDescription;
  const Perspective(
      {required this.id,
      required this.situation,
      required this.person,
      this.observation,
      this.additionalNotes,
      required this.isAppreciation,
      required this.ownDescription});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['situation'] = Variable<int>(situation);
    map['person'] = Variable<int>(person);
    if (!nullToAbsent || observation != null) {
      map['observation'] = Variable<String>(observation);
    }
    if (!nullToAbsent || additionalNotes != null) {
      map['additional_notes'] = Variable<String>(additionalNotes);
    }
    map['is_appreciation'] = Variable<bool>(isAppreciation);
    map['own_description'] = Variable<bool>(ownDescription);
    return map;
  }

  PerspectivesCompanion toCompanion(bool nullToAbsent) {
    return PerspectivesCompanion(
      id: Value(id),
      situation: Value(situation),
      person: Value(person),
      observation: observation == null && nullToAbsent
          ? const Value.absent()
          : Value(observation),
      additionalNotes: additionalNotes == null && nullToAbsent
          ? const Value.absent()
          : Value(additionalNotes),
      isAppreciation: Value(isAppreciation),
      ownDescription: Value(ownDescription),
    );
  }

  factory Perspective.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Perspective(
      id: serializer.fromJson<int>(json['id']),
      situation: serializer.fromJson<int>(json['situation']),
      person: serializer.fromJson<int>(json['person']),
      observation: serializer.fromJson<String?>(json['observation']),
      additionalNotes: serializer.fromJson<String?>(json['additionalNotes']),
      isAppreciation: serializer.fromJson<bool>(json['isAppreciation']),
      ownDescription: serializer.fromJson<bool>(json['ownDescription']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'situation': serializer.toJson<int>(situation),
      'person': serializer.toJson<int>(person),
      'observation': serializer.toJson<String?>(observation),
      'additionalNotes': serializer.toJson<String?>(additionalNotes),
      'isAppreciation': serializer.toJson<bool>(isAppreciation),
      'ownDescription': serializer.toJson<bool>(ownDescription),
    };
  }

  Perspective copyWith(
          {int? id,
          int? situation,
          int? person,
          Value<String?> observation = const Value.absent(),
          Value<String?> additionalNotes = const Value.absent(),
          bool? isAppreciation,
          bool? ownDescription}) =>
      Perspective(
        id: id ?? this.id,
        situation: situation ?? this.situation,
        person: person ?? this.person,
        observation: observation.present ? observation.value : this.observation,
        additionalNotes: additionalNotes.present
            ? additionalNotes.value
            : this.additionalNotes,
        isAppreciation: isAppreciation ?? this.isAppreciation,
        ownDescription: ownDescription ?? this.ownDescription,
      );
  @override
  String toString() {
    return (StringBuffer('Perspective(')
          ..write('id: $id, ')
          ..write('situation: $situation, ')
          ..write('person: $person, ')
          ..write('observation: $observation, ')
          ..write('additionalNotes: $additionalNotes, ')
          ..write('isAppreciation: $isAppreciation, ')
          ..write('ownDescription: $ownDescription')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, situation, person, observation,
      additionalNotes, isAppreciation, ownDescription);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Perspective &&
          other.id == this.id &&
          other.situation == this.situation &&
          other.person == this.person &&
          other.observation == this.observation &&
          other.additionalNotes == this.additionalNotes &&
          other.isAppreciation == this.isAppreciation &&
          other.ownDescription == this.ownDescription);
}

class PerspectivesCompanion extends UpdateCompanion<Perspective> {
  final Value<int> id;
  final Value<int> situation;
  final Value<int> person;
  final Value<String?> observation;
  final Value<String?> additionalNotes;
  final Value<bool> isAppreciation;
  final Value<bool> ownDescription;
  const PerspectivesCompanion({
    this.id = const Value.absent(),
    this.situation = const Value.absent(),
    this.person = const Value.absent(),
    this.observation = const Value.absent(),
    this.additionalNotes = const Value.absent(),
    this.isAppreciation = const Value.absent(),
    this.ownDescription = const Value.absent(),
  });
  PerspectivesCompanion.insert({
    this.id = const Value.absent(),
    required int situation,
    required int person,
    this.observation = const Value.absent(),
    this.additionalNotes = const Value.absent(),
    required bool isAppreciation,
    required bool ownDescription,
  })  : situation = Value(situation),
        person = Value(person),
        isAppreciation = Value(isAppreciation),
        ownDescription = Value(ownDescription);
  static Insertable<Perspective> custom({
    Expression<int>? id,
    Expression<int>? situation,
    Expression<int>? person,
    Expression<String>? observation,
    Expression<String>? additionalNotes,
    Expression<bool>? isAppreciation,
    Expression<bool>? ownDescription,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (situation != null) 'situation': situation,
      if (person != null) 'person': person,
      if (observation != null) 'observation': observation,
      if (additionalNotes != null) 'additional_notes': additionalNotes,
      if (isAppreciation != null) 'is_appreciation': isAppreciation,
      if (ownDescription != null) 'own_description': ownDescription,
    });
  }

  PerspectivesCompanion copyWith(
      {Value<int>? id,
      Value<int>? situation,
      Value<int>? person,
      Value<String?>? observation,
      Value<String?>? additionalNotes,
      Value<bool>? isAppreciation,
      Value<bool>? ownDescription}) {
    return PerspectivesCompanion(
      id: id ?? this.id,
      situation: situation ?? this.situation,
      person: person ?? this.person,
      observation: observation ?? this.observation,
      additionalNotes: additionalNotes ?? this.additionalNotes,
      isAppreciation: isAppreciation ?? this.isAppreciation,
      ownDescription: ownDescription ?? this.ownDescription,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (situation.present) {
      map['situation'] = Variable<int>(situation.value);
    }
    if (person.present) {
      map['person'] = Variable<int>(person.value);
    }
    if (observation.present) {
      map['observation'] = Variable<String>(observation.value);
    }
    if (additionalNotes.present) {
      map['additional_notes'] = Variable<String>(additionalNotes.value);
    }
    if (isAppreciation.present) {
      map['is_appreciation'] = Variable<bool>(isAppreciation.value);
    }
    if (ownDescription.present) {
      map['own_description'] = Variable<bool>(ownDescription.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PerspectivesCompanion(')
          ..write('id: $id, ')
          ..write('situation: $situation, ')
          ..write('person: $person, ')
          ..write('observation: $observation, ')
          ..write('additionalNotes: $additionalNotes, ')
          ..write('isAppreciation: $isAppreciation, ')
          ..write('ownDescription: $ownDescription')
          ..write(')'))
        .toString();
  }
}

class $PerspectiveEmotionsTable extends PerspectiveEmotions
    with TableInfo<$PerspectiveEmotionsTable, PerspectiveEmotion> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $PerspectiveEmotionsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _perspectiveMeta =
      const VerificationMeta('perspective');
  @override
  late final GeneratedColumn<int> perspective = GeneratedColumn<int>(
      'perspective', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES perspectives (id)'));
  static const VerificationMeta _emotionMeta =
      const VerificationMeta('emotion');
  @override
  late final GeneratedColumnWithTypeConverter<Emotion, String> emotion =
      GeneratedColumn<String>('emotion', aliasedName, false,
              type: DriftSqlType.string, requiredDuringInsert: true)
          .withConverter<Emotion>($PerspectiveEmotionsTable.$converteremotion);
  @override
  List<GeneratedColumn> get $columns => [perspective, emotion];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'perspective_emotions';
  @override
  VerificationContext validateIntegrity(Insertable<PerspectiveEmotion> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('perspective')) {
      context.handle(
          _perspectiveMeta,
          perspective.isAcceptableOrUnknown(
              data['perspective']!, _perspectiveMeta));
    } else if (isInserting) {
      context.missing(_perspectiveMeta);
    }
    context.handle(_emotionMeta, const VerificationResult.success());
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {perspective, emotion};
  @override
  PerspectiveEmotion map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return PerspectiveEmotion(
      perspective: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}perspective'])!,
      emotion: $PerspectiveEmotionsTable.$converteremotion.fromSql(
          attachedDatabase.typeMapping
              .read(DriftSqlType.string, data['${effectivePrefix}emotion'])!),
    );
  }

  @override
  $PerspectiveEmotionsTable createAlias(String alias) {
    return $PerspectiveEmotionsTable(attachedDatabase, alias);
  }

  static JsonTypeConverter2<Emotion, String, String> $converteremotion =
      const EnumNameConverter<Emotion>(Emotion.values);
}

class PerspectiveEmotion extends DataClass
    implements Insertable<PerspectiveEmotion> {
  final int perspective;
  final Emotion emotion;
  const PerspectiveEmotion({required this.perspective, required this.emotion});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['perspective'] = Variable<int>(perspective);
    {
      map['emotion'] = Variable<String>(
          $PerspectiveEmotionsTable.$converteremotion.toSql(emotion));
    }
    return map;
  }

  PerspectiveEmotionsCompanion toCompanion(bool nullToAbsent) {
    return PerspectiveEmotionsCompanion(
      perspective: Value(perspective),
      emotion: Value(emotion),
    );
  }

  factory PerspectiveEmotion.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return PerspectiveEmotion(
      perspective: serializer.fromJson<int>(json['perspective']),
      emotion: $PerspectiveEmotionsTable.$converteremotion
          .fromJson(serializer.fromJson<String>(json['emotion'])),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'perspective': serializer.toJson<int>(perspective),
      'emotion': serializer.toJson<String>(
          $PerspectiveEmotionsTable.$converteremotion.toJson(emotion)),
    };
  }

  PerspectiveEmotion copyWith({int? perspective, Emotion? emotion}) =>
      PerspectiveEmotion(
        perspective: perspective ?? this.perspective,
        emotion: emotion ?? this.emotion,
      );
  @override
  String toString() {
    return (StringBuffer('PerspectiveEmotion(')
          ..write('perspective: $perspective, ')
          ..write('emotion: $emotion')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(perspective, emotion);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is PerspectiveEmotion &&
          other.perspective == this.perspective &&
          other.emotion == this.emotion);
}

class PerspectiveEmotionsCompanion extends UpdateCompanion<PerspectiveEmotion> {
  final Value<int> perspective;
  final Value<Emotion> emotion;
  final Value<int> rowid;
  const PerspectiveEmotionsCompanion({
    this.perspective = const Value.absent(),
    this.emotion = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  PerspectiveEmotionsCompanion.insert({
    required int perspective,
    required Emotion emotion,
    this.rowid = const Value.absent(),
  })  : perspective = Value(perspective),
        emotion = Value(emotion);
  static Insertable<PerspectiveEmotion> custom({
    Expression<int>? perspective,
    Expression<String>? emotion,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (perspective != null) 'perspective': perspective,
      if (emotion != null) 'emotion': emotion,
      if (rowid != null) 'rowid': rowid,
    });
  }

  PerspectiveEmotionsCompanion copyWith(
      {Value<int>? perspective, Value<Emotion>? emotion, Value<int>? rowid}) {
    return PerspectiveEmotionsCompanion(
      perspective: perspective ?? this.perspective,
      emotion: emotion ?? this.emotion,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (perspective.present) {
      map['perspective'] = Variable<int>(perspective.value);
    }
    if (emotion.present) {
      map['emotion'] = Variable<String>(
          $PerspectiveEmotionsTable.$converteremotion.toSql(emotion.value));
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PerspectiveEmotionsCompanion(')
          ..write('perspective: $perspective, ')
          ..write('emotion: $emotion, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $PerspectiveNeedsTable extends PerspectiveNeeds
    with TableInfo<$PerspectiveNeedsTable, PerspectiveNeed> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $PerspectiveNeedsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _perspectiveMeta =
      const VerificationMeta('perspective');
  @override
  late final GeneratedColumn<int> perspective = GeneratedColumn<int>(
      'perspective', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES perspectives (id)'));
  static const VerificationMeta _needMeta = const VerificationMeta('need');
  @override
  late final GeneratedColumnWithTypeConverter<Need, String> need =
      GeneratedColumn<String>('need', aliasedName, false,
              type: DriftSqlType.string, requiredDuringInsert: true)
          .withConverter<Need>($PerspectiveNeedsTable.$converterneed);
  @override
  List<GeneratedColumn> get $columns => [perspective, need];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'perspective_needs';
  @override
  VerificationContext validateIntegrity(Insertable<PerspectiveNeed> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('perspective')) {
      context.handle(
          _perspectiveMeta,
          perspective.isAcceptableOrUnknown(
              data['perspective']!, _perspectiveMeta));
    } else if (isInserting) {
      context.missing(_perspectiveMeta);
    }
    context.handle(_needMeta, const VerificationResult.success());
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {perspective, need};
  @override
  PerspectiveNeed map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return PerspectiveNeed(
      perspective: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}perspective'])!,
      need: $PerspectiveNeedsTable.$converterneed.fromSql(attachedDatabase
          .typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}need'])!),
    );
  }

  @override
  $PerspectiveNeedsTable createAlias(String alias) {
    return $PerspectiveNeedsTable(attachedDatabase, alias);
  }

  static JsonTypeConverter2<Need, String, String> $converterneed =
      const EnumNameConverter<Need>(Need.values);
}

class PerspectiveNeed extends DataClass implements Insertable<PerspectiveNeed> {
  final int perspective;
  final Need need;
  const PerspectiveNeed({required this.perspective, required this.need});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['perspective'] = Variable<int>(perspective);
    {
      map['need'] =
          Variable<String>($PerspectiveNeedsTable.$converterneed.toSql(need));
    }
    return map;
  }

  PerspectiveNeedsCompanion toCompanion(bool nullToAbsent) {
    return PerspectiveNeedsCompanion(
      perspective: Value(perspective),
      need: Value(need),
    );
  }

  factory PerspectiveNeed.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return PerspectiveNeed(
      perspective: serializer.fromJson<int>(json['perspective']),
      need: $PerspectiveNeedsTable.$converterneed
          .fromJson(serializer.fromJson<String>(json['need'])),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'perspective': serializer.toJson<int>(perspective),
      'need': serializer
          .toJson<String>($PerspectiveNeedsTable.$converterneed.toJson(need)),
    };
  }

  PerspectiveNeed copyWith({int? perspective, Need? need}) => PerspectiveNeed(
        perspective: perspective ?? this.perspective,
        need: need ?? this.need,
      );
  @override
  String toString() {
    return (StringBuffer('PerspectiveNeed(')
          ..write('perspective: $perspective, ')
          ..write('need: $need')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(perspective, need);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is PerspectiveNeed &&
          other.perspective == this.perspective &&
          other.need == this.need);
}

class PerspectiveNeedsCompanion extends UpdateCompanion<PerspectiveNeed> {
  final Value<int> perspective;
  final Value<Need> need;
  final Value<int> rowid;
  const PerspectiveNeedsCompanion({
    this.perspective = const Value.absent(),
    this.need = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  PerspectiveNeedsCompanion.insert({
    required int perspective,
    required Need need,
    this.rowid = const Value.absent(),
  })  : perspective = Value(perspective),
        need = Value(need);
  static Insertable<PerspectiveNeed> custom({
    Expression<int>? perspective,
    Expression<String>? need,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (perspective != null) 'perspective': perspective,
      if (need != null) 'need': need,
      if (rowid != null) 'rowid': rowid,
    });
  }

  PerspectiveNeedsCompanion copyWith(
      {Value<int>? perspective, Value<Need>? need, Value<int>? rowid}) {
    return PerspectiveNeedsCompanion(
      perspective: perspective ?? this.perspective,
      need: need ?? this.need,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (perspective.present) {
      map['perspective'] = Variable<int>(perspective.value);
    }
    if (need.present) {
      map['need'] = Variable<String>(
          $PerspectiveNeedsTable.$converterneed.toSql(need.value));
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PerspectiveNeedsCompanion(')
          ..write('perspective: $perspective, ')
          ..write('need: $need, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $PerspectiveStrategiesTable extends PerspectiveStrategies
    with TableInfo<$PerspectiveStrategiesTable, PerspectiveStrategy> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $PerspectiveStrategiesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _perspectiveMeta =
      const VerificationMeta('perspective');
  @override
  late final GeneratedColumn<int> perspective = GeneratedColumn<int>(
      'perspective', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES perspectives (id)'));
  static const VerificationMeta _strategyMeta =
      const VerificationMeta('strategy');
  @override
  late final GeneratedColumn<int> strategy = GeneratedColumn<int>(
      'strategy', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES strategies (id)'));
  @override
  List<GeneratedColumn> get $columns => [perspective, strategy];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'perspective_strategies';
  @override
  VerificationContext validateIntegrity(
      Insertable<PerspectiveStrategy> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('perspective')) {
      context.handle(
          _perspectiveMeta,
          perspective.isAcceptableOrUnknown(
              data['perspective']!, _perspectiveMeta));
    } else if (isInserting) {
      context.missing(_perspectiveMeta);
    }
    if (data.containsKey('strategy')) {
      context.handle(_strategyMeta,
          strategy.isAcceptableOrUnknown(data['strategy']!, _strategyMeta));
    } else if (isInserting) {
      context.missing(_strategyMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {perspective, strategy};
  @override
  PerspectiveStrategy map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return PerspectiveStrategy(
      perspective: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}perspective'])!,
      strategy: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}strategy'])!,
    );
  }

  @override
  $PerspectiveStrategiesTable createAlias(String alias) {
    return $PerspectiveStrategiesTable(attachedDatabase, alias);
  }
}

class PerspectiveStrategy extends DataClass
    implements Insertable<PerspectiveStrategy> {
  final int perspective;
  final int strategy;
  const PerspectiveStrategy(
      {required this.perspective, required this.strategy});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['perspective'] = Variable<int>(perspective);
    map['strategy'] = Variable<int>(strategy);
    return map;
  }

  PerspectiveStrategiesCompanion toCompanion(bool nullToAbsent) {
    return PerspectiveStrategiesCompanion(
      perspective: Value(perspective),
      strategy: Value(strategy),
    );
  }

  factory PerspectiveStrategy.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return PerspectiveStrategy(
      perspective: serializer.fromJson<int>(json['perspective']),
      strategy: serializer.fromJson<int>(json['strategy']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'perspective': serializer.toJson<int>(perspective),
      'strategy': serializer.toJson<int>(strategy),
    };
  }

  PerspectiveStrategy copyWith({int? perspective, int? strategy}) =>
      PerspectiveStrategy(
        perspective: perspective ?? this.perspective,
        strategy: strategy ?? this.strategy,
      );
  @override
  String toString() {
    return (StringBuffer('PerspectiveStrategy(')
          ..write('perspective: $perspective, ')
          ..write('strategy: $strategy')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(perspective, strategy);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is PerspectiveStrategy &&
          other.perspective == this.perspective &&
          other.strategy == this.strategy);
}

class PerspectiveStrategiesCompanion
    extends UpdateCompanion<PerspectiveStrategy> {
  final Value<int> perspective;
  final Value<int> strategy;
  final Value<int> rowid;
  const PerspectiveStrategiesCompanion({
    this.perspective = const Value.absent(),
    this.strategy = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  PerspectiveStrategiesCompanion.insert({
    required int perspective,
    required int strategy,
    this.rowid = const Value.absent(),
  })  : perspective = Value(perspective),
        strategy = Value(strategy);
  static Insertable<PerspectiveStrategy> custom({
    Expression<int>? perspective,
    Expression<int>? strategy,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (perspective != null) 'perspective': perspective,
      if (strategy != null) 'strategy': strategy,
      if (rowid != null) 'rowid': rowid,
    });
  }

  PerspectiveStrategiesCompanion copyWith(
      {Value<int>? perspective, Value<int>? strategy, Value<int>? rowid}) {
    return PerspectiveStrategiesCompanion(
      perspective: perspective ?? this.perspective,
      strategy: strategy ?? this.strategy,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (perspective.present) {
      map['perspective'] = Variable<int>(perspective.value);
    }
    if (strategy.present) {
      map['strategy'] = Variable<int>(strategy.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PerspectiveStrategiesCompanion(')
          ..write('perspective: $perspective, ')
          ..write('strategy: $strategy, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(e);
  late final $SituationsTable situations = $SituationsTable(this);
  late final $PeopleTable people = $PeopleTable(this);
  late final $StrategiesTable strategies = $StrategiesTable(this);
  late final $StrategyNeedsTable strategyNeeds = $StrategyNeedsTable(this);
  late final $PerspectivesTable perspectives = $PerspectivesTable(this);
  late final $PerspectiveEmotionsTable perspectiveEmotions =
      $PerspectiveEmotionsTable(this);
  late final $PerspectiveNeedsTable perspectiveNeeds =
      $PerspectiveNeedsTable(this);
  late final $PerspectiveStrategiesTable perspectiveStrategies =
      $PerspectiveStrategiesTable(this);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        situations,
        people,
        strategies,
        strategyNeeds,
        perspectives,
        perspectiveEmotions,
        perspectiveNeeds,
        perspectiveStrategies
      ];
}
