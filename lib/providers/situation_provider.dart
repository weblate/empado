// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/enums/emotion.dart';
import 'package:empado/enums/need.dart';
import 'package:empado/models/editable_perspective.dart';
import 'package:empado/models/editable_situation.dart';
import 'package:empado/models/temporary_person.dart';
import 'package:empado/providers/journal_view_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class _SituationNotifier extends ChangeNotifier {
  final state = EditableSituation();

  void setTitle(String? title) {
    state.title = title;
    notifyListeners();
  }

  void addPerspective(TemporaryPerson person) {
    state.perspectives
        .putIfAbsent(person, () => EditablePerspective(person: person));
    notifyListeners();
  }

  void removePerspective(TemporaryPerson person) {
    state.perspectives.remove(person);
    notifyListeners();
  }

  void addEmotion(TemporaryPerson person, Emotion emotion) {
    final perspective = state.perspectives[person]!..addEmotion(emotion);
    if (perspective.needs.isEmpty) {
      perspective.isAppreciation = _isAppreciation(perspective);
    }
    notifyListeners();
  }

  void removeEmotion(TemporaryPerson person, Emotion emotion) {
    final perspective = state.perspectives[person]!..removeEmotion(emotion);
    if (perspective.needs.isEmpty) {
      perspective.isAppreciation = _isAppreciation(perspective);
    }
    notifyListeners();
  }

  bool _isAppreciation(EditablePerspective perspective) =>
      perspective.getPleasantEmotionCachedCount() >
      perspective.getUnpleasantEmotionCachedCount();

  void addNeed(TemporaryPerson person, Need need) {
    state.perspectives[person]!.needs.add(need);
    notifyListeners();
  }

  void removeNeed(TemporaryPerson person, Need need) {
    state.perspectives[person]!.needs.remove(need);
    notifyListeners();
  }

  void setOwnDescription(TemporaryPerson person, bool value) {
    state.perspectives[person]!.ownDescription = value;
    notifyListeners();
  }

  void setAsAppreciation(TemporaryPerson person, bool value) {
    state.perspectives[person]!.isAppreciation = value;
    notifyListeners();
  }

  void setObservation(TemporaryPerson person, String? observation) {
    if (observation?.isEmpty ?? true) {
      state.perspectives[person]!.observation = null;
    } else {
      state.perspectives[person]!.observation = observation;
    }
    notifyListeners();
  }

  void setStrategies(TemporaryPerson person, String? strategies) {
    state.perspectives[person]!.setStrategies(strategies);
    notifyListeners();
  }

  void setNotes(TemporaryPerson person, String? notes) {
    if (notes?.isEmpty ?? true) {
      state.perspectives[person]!.additionalNotes = null;
    } else {
      state.perspectives[person]!.additionalNotes = notes;
    }
    notifyListeners();
  }

  void resetState(WidgetRef ref) {
    ref
        .read(journalViewStateProvider.notifier)
        .resetState(); // must be first, as otherwise might point to non-existent perspective
    state.clear();
    notifyListeners();
  }
}

final situationProvider = ChangeNotifierProvider<_SituationNotifier>((ref) {
  return _SituationNotifier();
});
