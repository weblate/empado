// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:core';
import 'dart:math';
import 'dart:ui';

import 'package:empado/enums/emotion.dart';
import 'package:empado/enums/emotion_category.dart';
import 'package:empado/enums/emotion_intensity.dart';
import 'package:empado/screens/journal/components/emotion_picker/emotion_background_painter.dart';
import 'package:empado/screens/journal/components/emotion_picker/emotion_handle.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EmotionSliders extends StatefulWidget {
  final Function(Emotion?) onDrag;
  final Function(Emotion?) onValueSet;

  const EmotionSliders(
      {required this.onDrag, required this.onValueSet, super.key});

  @override
  State<EmotionSliders> createState() => _EmotionSlidersState();
}

class _EmotionSlidersState extends State<EmotionSliders> {
  static const _defaultEmotionIntensity = EmotionIntensity.lowest;
  static const _categoryIndexOffset = 6;
  static final _emotionCategoryCount = EmotionCategory.values.length;
  static final _emotionArc = 2 * pi / _emotionCategoryCount;

  _DragVariables? _dragVariables;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: EmotionBackgroundPainter(
          categoryIndexOffset: _categoryIndexOffset,
          curEmotionCategory: _dragVariables?.emotionCategory),
      child: Stack(
        children: EmotionCategory.values.map((ec) {
          final emotionHandleVisible =
              (_dragVariables == null || _dragVariables?.emotionCategory == ec);
          // either not dragging at all or currently dragging this emotion category

          return Visibility(
              //TODO could replace with SizedBox, if I figure out why that doesn't work for horizontal drags
              visible: emotionHandleVisible,
              child: Align(
                  alignment: _getHandleFractionalOffset(
                      ec, emotionHandleVisible ? _dragVariables : null),
                  child: GestureDetector(
                    supportedDevices: const {
                      PointerDeviceKind.mouse,
                      PointerDeviceKind.stylus,
                      PointerDeviceKind.invertedStylus,
                      PointerDeviceKind.touch,
                      PointerDeviceKind.unknown
                    }, // disables trackpad swipes for dragging
                    onTap: () {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          duration: const Duration(seconds: 2),
                          content: Text(
                              AppLocalizations.of(context).noEmotionsHint)));
                    },
                    onHorizontalDragStart:
                        (details) => //TODO onPan doesn't work, but perhaps there'd be a better way than overriding horizontal drags only?
                            _onPanStart(details, ec),
                    onHorizontalDragUpdate: _onPanUpdate,
                    onHorizontalDragEnd: _onPanEnd,
                    child: EmotionHandle(ec),
                  )));
        }).toList(),
      ),
    );
  }

  void _onPanStart(DragStartDetails details, EmotionCategory emotionCategory) {
    setState(() {
      _dragVariables = _DragVariables(emotionCategory,
          _getEmotionAngleRads(emotionCategory), _defaultEmotionIntensity);
    });

    widget.onDrag(Emotion.fromCategoryAndIntensity(
        emotionCategory, _defaultEmotionIntensity));
  }

  void _onPanUpdate(DragUpdateDetails details) {
    final maxIntensity = EmotionIntensity.values.length - 1;

    final renderBox = context.findRenderObject() as RenderBox;
    final height = renderBox.size.height;
    final localCoords = renderBox.globalToLocal(details.globalPosition);
    final y = localCoords.dy;

    if (y > height) {
      // Dragging below slider area = cancel area

      if (_dragVariables?.emotionIntensity != null) {
        setState(() {
          _dragVariables?.emotionIntensity = null;
          widget.onDrag(null);
        });
      }
    } else {
      // Dragging inside slider area = changing intensity

      final width = renderBox.size.width;

      final trackEndToStart = (_dragVariables!.horizDrag
          ? -width * _dragVariables!.dragDirectionCosSin
          : -height * _dragVariables!.dragDirectionCosSin);
      // positive: track goes from right to left (or top to bottom)
      // negative: track goes from left to right (or bottom to top)
      final trackStart = _dragVariables!.horizDrag
          ? (width - trackEndToStart) / 2
          : (height - trackEndToStart) / 2;
      // e.g. for anger or excitement: trackStart = 0
      // e.g. for disgust or shame: trackStart = width

      final pos = _dragVariables!.horizDrag ? localCoords.dx : y;
      final trackPercentage =
          ((pos - trackStart) / trackEndToStart).clamp(0, 1);

      final intensityIndex = (trackPercentage * (maxIntensity)).round();
      final intensity = EmotionIntensity.values[intensityIndex];

      if (_dragVariables?.emotionIntensity != intensity) {
        setState(() {
          _dragVariables?.emotionIntensity = intensity;
          widget.onDrag(Emotion.fromCategoryAndIntensity(
              _dragVariables!.emotionCategory, intensity));
        });
      }
    }
  }

  void _onPanEnd(DragEndDetails details) {
    setState(() {
      final intensity = _dragVariables?.emotionIntensity;
      if (intensity != null) {
        widget.onValueSet(Emotion.fromCategoryAndIntensity(
            _dragVariables!.emotionCategory, intensity));
      } else {
        widget.onValueSet(null);
      }
      _dragVariables = null;
    });
  }

  FractionalOffset _getHandleFractionalOffset(
      EmotionCategory emotionCategory, _DragVariables? dragConstants) {
    final angleRads = _getEmotionAngleRads(emotionCategory);

    if (dragConstants != null) {
      if (dragConstants.emotionIntensity == null) {
        return const FractionalOffset(
            0.5, 50); //this hides the handle offscreen
      } else {
        final intensityFraction = dragConstants.emotionIntensity!.index /
            (EmotionIntensity.values.length - 1);
        final intensityNeg1_1 = (intensityFraction * 2) - 1;

        final x = _moveNeg1_1To0_1(-intensityNeg1_1 * cos(angleRads));
        final y = _moveNeg1_1To0_1(-intensityNeg1_1 * -sin(angleRads));

        return FractionalOffset(x, y);
      }
    } else {
      //TODO cache
      final x = _moveNeg1_1To0_1(cos(angleRads));
      final y = _moveNeg1_1To0_1(-sin(angleRads));
      return FractionalOffset(x, y);
    }
  }

  double _moveNeg1_1To0_1(double d) => d / 2 + 0.5;

  double _getEmotionAngleRads(EmotionCategory emotionCategory) =>
      -((emotionCategory.index + _categoryIndexOffset) %
          _emotionCategoryCount) *
      _emotionArc; // negative, because we're going clockwise, from disgust to joy
}

class _DragVariables {
  final EmotionCategory emotionCategory;
  late final bool horizDrag;
  late final double dragDirectionCosSin;
  EmotionIntensity? emotionIntensity;

  _DragVariables(
      this.emotionCategory, double categoryAngle, this.emotionIntensity) {
    horizDrag = (categoryAngle + pi / 4) % pi <= pi / 2;
    dragDirectionCosSin = horizDrag ? cos(categoryAngle) : -sin(categoryAngle);
  }
}
