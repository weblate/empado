// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ContextCard extends StatelessWidget {
  final Widget fullScreenDialog;
  final String title;
  final String subtitle;
  final String assetPath;
  final String? content;
  final Function() onDelete;

  const ContextCard(
      {required this.title,
      required this.subtitle,
      required this.assetPath,
      required this.fullScreenDialog,
      required this.onDelete,
      this.content,
      super.key});

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Card(
            clipBehavior: Clip.hardEdge,
            color: colorScheme.surface,
            surfaceTintColor: Colors.white,
            child: InkWell(
                onTap: () async {
                  await Navigator.of(context).push(MaterialPageRoute(
                      builder: (_) => fullScreenDialog,
                      fullscreenDialog: true));
                },
                child: content == null
                    ? _ContextCardRow(
                        title: title,
                        subtitle: subtitle,
                        assetPath: assetPath,
                        hasContent: false)
                    : Column(children: [
                        _ContextCardRow(
                            title: title,
                            subtitle: subtitle,
                            assetPath: assetPath,
                            hasContent: true),
                        Container(
                            padding: const EdgeInsets.all(16),
                            width: double.infinity,
                            color: colorScheme.surfaceVariant,
                            child: Row(children: [
                              Expanded(
                                  child: Text(
                                content!,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium
                                    ?.copyWith(
                                        color: colorScheme.onSurfaceVariant),
                              )),
                              IconButton(
                                  onPressed: onDelete,
                                  icon: const Icon(Icons.delete))
                            ]))
                      ]))));
  }
}

class _ContextCardRow extends StatelessWidget {
  final String title;
  final String subtitle;
  final String assetPath;
  final bool hasContent;

  const _ContextCardRow(
      {required this.title,
      required this.subtitle,
      required this.assetPath,
      required this.hasContent});

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 12),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            SvgPicture.asset(assetPath, width: 48),
            const SizedBox(width: 12),
            Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                  Text(title, style: textTheme.titleMedium),
                  Text(subtitle, style: textTheme.bodyMedium)
                ])),
            const SizedBox(width: 12),
            Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: hasContent
                      ? colorScheme.surfaceVariant
                      : colorScheme.primary,
                ),
                child: hasContent
                    ? Icon(Icons.edit, color: colorScheme.onSurfaceVariant)
                    : Icon(Icons.add, color: colorScheme.onPrimary))
          ],
        ));
  }
}
