// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';

class SmallRadio<T> extends StatelessWidget {
  final T value;
  final T groupValue;
  final Function(T?)? onChanged;
  final Widget child;
  final EdgeInsetsDirectional padding;

  const SmallRadio(
      {required this.value,
      required this.groupValue,
      required this.child,
      required this.padding,
      this.onChanged,
      super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        borderRadius: BorderRadius.circular(8),
        onTap: () => onChanged?.call(value),
        child: Ink(
            padding: padding,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Radio<T>.adaptive(
                    value: value,
                    groupValue: groupValue,
                    onChanged: onChanged,
                    activeColor: Theme.of(context).colorScheme.primary),
                const SizedBox(width: 4),
                child
              ],
            )));
  }
}
