// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/db/database.dart';
import 'package:empado/models/temporary_person.dart';
import 'package:empado/providers/journal_view_provider.dart';
import 'package:empado/providers/situation_provider.dart';
import 'package:empado/screens/journal/components/add_person_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class PersonPicker extends ConsumerWidget {
  const PersonPicker({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final currentPerson = ref
        .watch(journalViewStateProvider.select((value) => value.currentPerson));
    final peopleInvolved = ref.watch(situationProvider).state.perspectives.keys;
    final homeViewStateNotifier = ref.read(journalViewStateProvider.notifier);
    final situationNotifier = ref.read(situationProvider.notifier);

    return ListView(children: [
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          child: Text(
            AppLocalizations.of(context).whoWasInvolved,
            style: Theme.of(context).textTheme.titleLarge,
            textAlign: TextAlign.center,
          )),
      ...peopleInvolved.map((p) {
        return RadioListTile.adaptive(
          groupValue: currentPerson,
          value: p,
          title: Text(p.name ?? AppLocalizations.of(context).you),
          activeColor: Theme.of(context).colorScheme.primary,
          subtitle: p.isYou() ? null : Text(p.pronoun.getTranslation(context)),
          secondary: p.isYou()
              ? null
              : IconButton(
                  tooltip: AppLocalizations.of(context).remove,
                  icon: const Icon(Icons.delete),
                  onPressed: () {
                    if (currentPerson == p) {
                      homeViewStateNotifier
                          .setCurrentPerson(AppDatabase.temporaryYouPerson);
                    }
                    situationNotifier.removePerspective(p);
                  }),
          onChanged: (TemporaryPerson? person) {
            if (person != null && person != currentPerson) {
              homeViewStateNotifier.setCurrentPerson(person);
            }
          },
        );
      }),
      ListTile(
          title: Text(AppLocalizations.of(context).addPersonToSituation),
          leading: const Icon(Icons.add),
          onTap: () async {
            await showDialog(
                context: context,
                builder: (context) => const AddPersonDialog());
          })
    ]);
  }
}
