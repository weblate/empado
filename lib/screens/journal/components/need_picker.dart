// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/enums/need_category.dart';
import 'package:empado/models/temporary_person.dart';
import 'package:empado/providers/situation_provider.dart';
import 'package:empado/screens/journal/components/situation_sheet/situation_sheet.dart';
import 'package:empado/utils/theme_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NeedPicker extends ConsumerWidget {
  final TemporaryPerson person;

  const NeedPicker({required this.person, super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final perspective =
        ref.watch(situationProvider).state.perspectives[person]!;
    final situationNotifier = ref.read(situationProvider.notifier);

    final chosenNeeds = perspective.needs;
    final needGroups = NeedCategory.groups;

    return ListView(
      padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewPadding.bottom +
              SituationSheet.height),
      children: [
        if (perspective.isAppreciation)
          Padding(
              padding: const EdgeInsets.all(16),
              child: Text(
                person.isYou()
                    ? AppLocalizations.of(context).meetingNeedsForYou
                    : AppLocalizations.of(context).meetingNeedsForThey(
                        person.name!, person.pronoun.localeKey),
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium
                    ?.copyWith(fontVariations: ThemeUtil.boldVariation),
              )),
        ...needGroups.entries.map((mapEntry) {
          final needCategory = mapEntry.key;
          final needList = mapEntry.value;

          return ExpansionTile(
            key: PageStorageKey(needCategory),
            backgroundColor: Theme.of(context).colorScheme.surfaceVariant,
            textColor: Theme.of(context).colorScheme.onSurfaceVariant,
            leading: SvgPicture.asset(
              needCategory.assetPath,
              width: 48,
            ),
            title: Text(needCategory.getTranslation(context)),
            subtitle: Text(
              needList
                  .map((need) => need.getTranslation(context))
                  .join(AppLocalizations.of(context).listJoiner),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            children: needList
                .map((need) => CheckboxListTile(
                      value: chosenNeeds.contains(need),
                      title: Text(need.getTranslation(context)),
                      onChanged: (bool? value) {
                        if (value == true) {
                          situationNotifier.addNeed(person, need);
                        } else {
                          situationNotifier.removeNeed(person, need);
                        }
                      },
                    ))
                .toList(),
          );
        })
      ],
    );
  }
}
