// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/components/perspective_statement/active_perspective_statement.dart';
import 'package:empado/components/perspective_statement/inactive_perspective_statement.dart';
import 'package:empado/components/perspective_statement/perspective_column.dart';
import 'package:empado/providers/db_provider.dart';
import 'package:empado/providers/history_provider.dart';
import 'package:empado/providers/journal_view_provider.dart';
import 'package:empado/providers/situation_provider.dart';
import 'package:empado/providers/strategy_provider.dart';
import 'package:empado/screens/journal/components/situation_sheet/title_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ExpandedContent extends ConsumerWidget {
  // TODO use Esc to close

  final Future? Function() collapseSheet;

  const ExpandedContent({required this.collapseSheet, super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final currentPerson = ref
        .watch(journalViewStateProvider.select((value) => value.currentPerson));
    final situation = ref.watch(situationProvider).state;
    final situationNotifier = ref.read(situationProvider.notifier);

    final colorScheme = Theme.of(context).colorScheme;

    return Scaffold(
        backgroundColor: colorScheme.secondaryContainer,
        body: RefreshIndicator(
            //TODO this is a hacky solution, should probably find a custom one; perhaps https://pub.dev/packages/overscroll_pop ?
            edgeOffset: double.negativeInfinity,
            onRefresh: () async => collapseSheet(),
            child: CustomScrollView(
              slivers: [
                SliverAppBar(
                    centerTitle: true,
                    backgroundColor: Colors.transparent,
                    systemOverlayStyle:
                        Theme.of(context).brightness == Brightness.light
                            ? SystemUiOverlayStyle.dark
                            : SystemUiOverlayStyle.light,
                    leading: IconButton(
                        tooltip:
                            AppLocalizations.of(context).closeSituationOverview,
                        onPressed: collapseSheet,
                        icon: const Icon(Icons.expand_more)),
                    title: InkWell(
                        borderRadius: BorderRadius.circular(8),
                        onTap: () async {
                          await showDialog(
                              context: context,
                              builder: (context) => TitleDialog(
                                  initialValue: situation.title,
                                  onSetTitle: situationNotifier.setTitle));
                        },
                        child: Ink(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 8),
                            child:
                                Row(mainAxisSize: MainAxisSize.min, children: [
                              if (situation.title == null)
                                Text(
                                    AppLocalizations.of(context)
                                        .untitledSituation,
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleLarge
                                        ?.copyWith(
                                            color: colorScheme.onSurfaceVariant
                                                .withAlpha(191)))
                              else
                                Text(situation.title!,
                                    style:
                                        Theme.of(context).textTheme.titleLarge),
                              const SizedBox(width: 8),
                              Icon(Icons.edit,
                                  size: 18,
                                  color: situation.title == null
                                      ? colorScheme.onSurfaceVariant
                                          .withAlpha(191)
                                      : colorScheme.onSurfaceVariant)
                            ])))),
                ...situation.perspectives.values.map((perspective) {
                  final person = perspective.person;
                  final isActive = currentPerson == perspective.person;

                  return SliverToBoxAdapter(
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(
                              16 + MediaQuery.of(context).viewPadding.left,
                              0,
                              16 + MediaQuery.of(context).viewPadding.right,
                              0),
                          child: Card(
                              color: isActive
                                  ? colorScheme.background
                                  : colorScheme.surfaceVariant,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 24),
                                child: PerspectiveColumn(
                                    isAppreciation: perspective.isAppreciation,
                                    perspectiveStatement: (isActive)
                                        ? ActivePerspectiveStatement(
                                            perspective: perspective,
                                            removeEmotion: (e) =>
                                                situationNotifier.removeEmotion(
                                                    person, e),
                                            removeNeed: (n) => situationNotifier
                                                .removeNeed(person, n),
                                            setAsAppreciation: (value) =>
                                                situationNotifier
                                                    .setAsAppreciation(
                                                        person, value))
                                        : InactivePerspectiveStatement(
                                            perspective: perspective),
                                    observation: perspective.observation,
                                    strategies: perspective.strategies
                                        .map((sc) => sc.strategy.value),
                                    additionalNotes: perspective.additionalNotes),
                              ))));
                }),
                const SliverToBoxAdapter(child: SizedBox(height: 16))
              ],
            )),
        bottomNavigationBar: Container(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  color: colorScheme.onSecondaryContainer.withAlpha(31),
                  width: 1.0,
                ),
              ),
            ),
            child: BottomAppBar(
                height: 56,
                color: Colors.transparent,
                surfaceTintColor: Colors.transparent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton.icon(
                        onPressed: () async {
                          situationNotifier.resetState(ref);
                        }, //TODO maybe allow undoing via snackbar?
                        icon: const Icon(Icons.delete),
                        label: Text(AppLocalizations.of(context)
                            .discard)), //TODO change to "Discard changes" if it's a saved one
                    TextButton.icon(
                        onPressed: () async {
                          // TODO check that each perspective has emotions or needs filled in
                          final scaffoldMessenger =
                              ScaffoldMessenger.of(context);
                          final localizations = AppLocalizations.of(context);
                          await ref
                              .read(dbProvider)
                              .createOrUpdateSituation(situation);
                          ref
                            ..invalidate(fullSituationHistoryProvider)
                            ..invalidate(strategyProvider);
                          situationNotifier.resetState(ref);
                          await collapseSheet();
                          scaffoldMessenger.showSnackBar(SnackBar(
                              duration: const Duration(seconds: 2),
                              content: Text(localizations.situationSaved)));
                        },
                        icon: const Icon(Icons.check),
                        label: Text(AppLocalizations.of(context).saveAndClear))
                  ],
                ))));
  }
}
