// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/components/emotion_chip.dart';
import 'package:empado/enums/emotion.dart';
import 'package:empado/enums/journal_tab.dart';
import 'package:empado/enums/need.dart';
import 'package:empado/screens/journal/components/situation_sheet/chip_row.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SituationSheetCollapsedContent extends StatelessWidget {
  final JournalTab homeTab;
  final Set<Emotion> emotions;
  final Set<Need> needs;
  final Function(Emotion) onRemoveEmotion;
  final Function(Need) onRemoveNeed;

  const SituationSheetCollapsedContent(
      {required this.homeTab,
      required this.emotions,
      required this.needs,
      required this.onRemoveEmotion,
      required this.onRemoveNeed,
      super.key});

  @override
  Widget build(BuildContext context) {
    switch (homeTab) {
      case JournalTab.emotions:
        if (emotions.isEmpty) {
          return _EmptyMessage(AppLocalizations.of(context).sheetUsageHint);
          //TODO show this hint somewhere else: AppLocalizations.of(context).noEmotionsHint
        } else {
          return ChipRow(
              children: emotions
                  .map((e) => Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 2),
                      child: EmotionChip(
                          emotion: e, onDeleted: () => onRemoveEmotion(e))))
                  .toList());
        }

      case JournalTab.needs:
        if (needs.isEmpty) {
          return _EmptyMessage(AppLocalizations.of(context).sheetUsageHint);
        } else {
          return ChipRow(
            children: needs
                .map((n) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 2),
                    child: Chip(
                        label: Text(n.getTranslation(context)),
                        onDeleted: () => onRemoveNeed(n))))
                .toList(),
          );
        }

      case JournalTab.situationContext:
        return _EmptyMessage(AppLocalizations.of(context).sheetUsageHint);
    }
  }
}

class _EmptyMessage extends StatelessWidget {
  final String message;

  const _EmptyMessage(this.message);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Text(message, textAlign: TextAlign.center)));
  }
}
