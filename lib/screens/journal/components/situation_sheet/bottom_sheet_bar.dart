// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// This file is derived from work covered by the following license notice:
//
//    MIT License
//
//    Copyright (c) 2020 Matt Hurst
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:measure_size/measure_size.dart';

/// A toolbar that aligns to the bottom of a widget and expands into a bottom
/// sheet.
class BottomSheetBar extends StatefulWidget {
  /// The minimum vertical speed (measured in pixels-per-second) required to
  /// collapse or expand the bottom-sheet with a fling gesture
  final double velocityMin;

  /// The toolbar will be aligned to the bottom of the body [Widget]. Padding
  /// equal to [height] is added to the bottom of this widget.
  final Widget body;

  /// A function to build the widget displayed when the bottom sheet is
  /// expanded. If the expanded content is scrollable, pass the provided
  /// [ScrollController] to the scrollable widget.
  final Widget Function(ScrollController) expandedBuilder;

  /// A [Widget] to be displayed on the toolbar in its collapsed state. When
  /// null, the toolbar will be empty.
  final Widget? collapsedContent;

  /// The height of the collapsed toolbar. Defaults to [kToolbarHeight] (56.0)
  final double height;

  /// A controller can be used to listen to events, and expand and collapse the
  /// bottom sheet.
  final BottomSheetBarController? controller;

  /// The background color of the toolbar and bottom sheet. Defaults to
  /// [Theme.of(context).bottomAppBarColor]
  final Color? color;

  /// Provide a border-radius to adjust the shape of the toolbar
  final BorderRadius? borderRadius;

  /// Provide a border-radius to adjust the shape of the bottom-sheet
  /// when expanded
  final BorderRadius? borderRadiusExpanded;

  /// Provide a box-shadow list to add to [Ink] widget
  final List<BoxShadow>? boxShadows;

  /// If [true], the bottom sheet can be dismissed by tapping elsewhere. Defaults
  /// to [true]
  final bool isDismissable;

  /// If [true], the bottom sheet cannot be opened or closed with a swipe gesture.
  /// Defaults to [true]
  final bool locked;

  /// If [true], the bottom sheet is not shown.
  /// Defaults to [false]
  final bool hidden;

  const BottomSheetBar({
    required this.body,
    required this.expandedBuilder,
    this.collapsedContent, //TODO reduce arguments
    this.controller,
    this.color,
    this.borderRadius,
    this.borderRadiusExpanded,
    this.boxShadows,
    this.height = kToolbarHeight,
    this.isDismissable = true,
    this.locked = true,
    this.velocityMin = 320.0,
    this.hidden = false,
    super.key,
  });

  @override
  State<BottomSheetBar> createState() => _BottomSheetBarState();
}

/// A controller used to expand or collapse the bottom sheet of a
/// [BottomSheetBar]. Listeners can be added to respond to expand and collapse
/// events. The expanded or collapsed state can also be determined through this
/// controller.
class BottomSheetBarController {
  AnimationController? _animationController;
  final _listeners = <Function()>[];

  /// Only returns [true] if the bottom sheet is fully collapsed
  bool get isCollapsed => _animationController?.value == 0.0;

  /// Only returns [true] if the bottom sheet is fully expanded
  bool get isExpanded => _animationController?.value == 1.0;

  /// Adds a function to be called on every animation frame
  void addListener(Function() listener) => _listeners.add(listener);

  /// Used internally to assign the [AnimationController] created by a
  /// [BottomSheetBar] to this controller. Unless you're using advanced
  /// animation techniques, you probably won't ever need to use this method.
  void attach(AnimationController animationController) {
    _animationController?.removeListener(_listener);
    _animationController = animationController;
    _animationController?.addListener(_listener);
  }

  /// Collapse the bottom sheet built by [BottomSheetBar.expandedBuilder]
  Future<void>? collapse() => _animationController
      ?.fling(velocity: -1.0)
      .then((_) => _animationController?.animateTo(0.0));

  /// Removes all previously added listeners
  void dispose() {
    for (final listener in _listeners) {
      removeListener(listener);
      _animationController?.removeListener(listener);
    }
  }

  /// Expand the bottom sheet built by [BottomSheetBar.expandedBuilder]
  Future<void>? expand() => _animationController
      ?.fling(velocity: 1.0)
      .then((_) => _animationController?.animateTo(1.0));

  /// Remove a previously added listener
  void removeListener(Function listener) => _listeners.remove(listener);

  void _listener() {
    for (final listener in _listeners) {
      listener.call();
    }
  }
}

class _BottomSheetBarState extends State<BottomSheetBar>
    with SingleTickerProviderStateMixin {
  final _scrollController = ScrollController();

  bool _isScrolled = false;
  Size _expandedSize = Size.zero;

  late AnimationController _animationController;
  late BottomSheetBarController _controller;

  double get _heightDiff => _expandedSize.height - widget.height;

  @override
  Widget build(BuildContext context) {
    final viewPadding = MediaQuery.of(context).viewPadding;
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, _) {
        return WillPopScope(
          onWillPop: _controller.isExpanded
              ? () async {
                  await _controller.collapse();
                  return false;
                }
              : null,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              // Body widget
              Positioned.fill(
                child: widget.body,
              ),

              /// Collapsed widget
              if (!widget.hidden)
                GestureDetector(
                  onVerticalDragUpdate: _eventMove,
                  onVerticalDragEnd: _eventEnd,
                  child: AnimatedBuilder(
                    animation: _animationController,
                    builder: (context, child) => Stack(
                      children: [
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: IgnorePointer(
                            ignoring: !_controller.isCollapsed,
                            child: GestureDetector(
                              onTap: _controller.expand,
                              child: Container(
                                padding: EdgeInsets.fromLTRB(viewPadding.left,
                                    0, viewPadding.right, viewPadding.bottom),
                                clipBehavior: Clip.hardEdge,
                                decoration: BoxDecoration(
                                  color: widget.color ??
                                      Theme.of(context)
                                          .bottomSheetTheme
                                          .backgroundColor,
                                  boxShadow: widget.boxShadows,
                                  borderRadius: BorderRadius.lerp(
                                    widget.borderRadius,
                                    widget.borderRadiusExpanded ??
                                        widget.borderRadius,
                                    _animationController.value,
                                  ),
                                ),
                                height:
                                    _animationController.value * _heightDiff +
                                        widget.height,
                                width: double.infinity,
                                child: FadeTransition(
                                  opacity: Tween(begin: 1.0, end: 0.0)
                                      .animate(_animationController),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        const SizedBox(height: 3),
                                        Center(
                                            child: Container(
                                                decoration: BoxDecoration(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4)),
                                                height: 3,
                                                width: 40)),
                                        if (widget.collapsedContent != null)
                                          Expanded(
                                              child: widget.collapsedContent!)
                                      ]),
                                ),
                              ),
                            ),
                          ),
                        ),

                        /// Expanded widget
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: IgnorePointer(
                            ignoring: _controller.isCollapsed,
                            child: FadeTransition(
                              opacity: Tween(begin: -13.0, end: 1.0)
                                  .animate(_animationController),
                              child: RepaintBoundary(
                                child: MeasureSize(
                                  onChange: (size) =>
                                      setState(() => _expandedSize = size),
                                  child:
                                      widget.expandedBuilder(_scrollController),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
            ],
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 250),
    );

    _scrollController.addListener(() {
      if (widget.locked || _isScrolled) return;
      _jumpToZero();
    });

    _controller = widget.controller ?? BottomSheetBarController();
    _controller.attach(_animationController);
  }

  void _eventEnd(DragEndDetails details) {
    final velocity = details.velocity;
    if (_animationController.isAnimating ||
        (_controller.isExpanded && _isScrolled)) {
      return;
    } else if (velocity.pixelsPerSecond.dy.abs() >= widget.velocityMin &&
        _heightDiff > 0) {
      _animationController.fling(
        velocity: -1 * (velocity.pixelsPerSecond.dy / _heightDiff),
      );
    } else if ((1 - _animationController.value) > _animationController.value) {
      unawaited(_controller.collapse());
    } else {
      unawaited(_controller.expand());
    }
  }

  void _eventMove(DragUpdateDetails details) {
    final dy = details.delta.dy;
    if (_heightDiff <= 0) {
      return;
    }
    if (!_isScrolled) {
      _animationController.value -= dy / _heightDiff;
    }

    if (!_scrollController.hasClients) {
      setState(() => _isScrolled = false);
    } else if (_controller.isExpanded && _scrollController.offset <= 0) {
      setState(() => _isScrolled = dy <= 0);
    } else if (_controller.isCollapsed) {
      _jumpToZero();
      setState(() => _isScrolled = false);
    }
  }

  void _jumpToZero() {
    if (_scrollController.hasClients) {
      _scrollController.jumpTo(0);
    }
  }
}
