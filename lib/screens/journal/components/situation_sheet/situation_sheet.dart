// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/models/temporary_person.dart';
import 'package:empado/providers/journal_view_provider.dart';
import 'package:empado/providers/situation_provider.dart';
import 'package:empado/screens/journal/components/situation_sheet/bottom_sheet_bar.dart';
import 'package:empado/screens/journal/components/situation_sheet/collapsed_content.dart';
import 'package:empado/screens/journal/components/situation_sheet/expanded_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SituationSheet extends ConsumerStatefulWidget {
  static const height = kToolbarHeight;
  final TemporaryPerson person;
  final Widget body;

  const SituationSheet({required this.person, required this.body, super.key});

  @override
  ConsumerState<SituationSheet> createState() => _SituationSheetState();
}

class _SituationSheetState extends ConsumerState<SituationSheet> {
  final _bsbController = BottomSheetBarController();

  @override
  void dispose() {
    _bsbController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final homeViewState = ref.watch(journalViewStateProvider);
    final curPerspective =
        ref.watch(situationProvider).state.perspectives[widget.person]!;
    final situationNotifier = ref.read(situationProvider.notifier);
    final colorScheme = Theme.of(context).colorScheme;

    return BottomSheetBar(
      hidden: homeViewState.dontDisplaySheet,
      height: SituationSheet.height + MediaQuery.of(context).viewPadding.bottom,
      color: colorScheme.secondaryContainer,
      // color: colorScheme.surfaceVariant,
      controller: _bsbController,
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(16.0),
        topRight: Radius.circular(16.0),
      ),
      borderRadiusExpanded: const BorderRadius.only(
        topLeft: Radius.circular(0.0),
        topRight: Radius.circular(0.0),
      ),
      boxShadows: [
        BoxShadow(
            color: Theme.of(context).colorScheme.shadow.withOpacity(0.25),
            blurRadius: 4.0),
      ],
      expandedBuilder: (_) =>
          ExpandedContent(collapseSheet: _bsbController.collapse),
      collapsedContent: SituationSheetCollapsedContent(
          homeTab: homeViewState.currentTab,
          emotions: curPerspective.emotions,
          needs: curPerspective.needs,
          onRemoveEmotion: (e) =>
              situationNotifier.removeEmotion(widget.person, e),
          onRemoveNeed: (n) => situationNotifier.removeNeed(widget.person, n)),
      body: widget.body,
    );
  }
}
