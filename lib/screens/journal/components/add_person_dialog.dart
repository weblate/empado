// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/components/error_content.dart';
import 'package:empado/db/database.dart';
import 'package:empado/enums/pronoun.dart';
import 'package:empado/models/temporary_person.dart';
import 'package:empado/providers/db_provider.dart';
import 'package:empado/providers/journal_view_provider.dart';
import 'package:empado/providers/situation_provider.dart';
import 'package:empado/screens/journal/components/small_radio.dart';
import 'package:empado/utils/layout_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AddPersonDialog extends ConsumerStatefulWidget {
  const AddPersonDialog({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _AddPersonDialogState();
}

class _AddPersonDialogState extends ConsumerState<AddPersonDialog> {
  final _formKey = GlobalKey<FormState>();
  late final TextEditingController _textEditingController;
  late final Future<List<Person>> _peopleFuture;

  bool addingNewPerson = false;
  Pronoun pronoun = Pronoun.they;

  @override
  void initState() {
    _textEditingController = TextEditingController();
    // ignore: discarded_futures
    _peopleFuture = ref.read(dbProvider).queryAllPeople();
    super.initState();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text(AppLocalizations.of(context).addPersonToSituation),
        content: SizedBox(
            width: LayoutUtil.dialogWidth,
            child: FutureBuilder(
                future: _peopleFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasData) {
                      final people = snapshot.data as List<Person>;
                      return Form(
                          key: _formKey,
                          child: ListView(
                            shrinkWrap: true,
                            children: [
                              ...people
                                  .map((p) => TemporaryPerson.personIsYou(p)
                                      ? const SizedBox()
                                      : ListTile(
                                          contentPadding: EdgeInsets.zero,
                                          title: Text(p.name!),
                                          trailing: Text(p.pronoun
                                              .getTranslation(context)),
                                          onTap: () => _setPersonAndClose(
                                              TemporaryPerson.fromPerson(p)),
                                        )),
                              if (people.length >
                                  1) // if there's a person other than you
                                const Divider(),
                              ListTile(
                                contentPadding: EdgeInsets.zero,
                                leading: const Icon(Icons.add),
                                title: Text(
                                    AppLocalizations.of(context).newPerson),
                                trailing: Icon(addingNewPerson
                                    ? Icons.expand_less
                                    : Icons.expand_more),
                                onTap: () => setState(() {
                                  addingNewPerson = !addingNewPerson;
                                }),
                              ),
                              if (addingNewPerson) ...[
                                const SizedBox(height: 12),
                                TextFormField(
                                    autofocus: true,
                                    controller: _textEditingController,
                                    decoration: InputDecoration(
                                        labelText:
                                            AppLocalizations.of(context).name),
                                    validator: (text) {
                                      if (text == null || text.trim().isEmpty) {
                                        return AppLocalizations.of(context)
                                            .mustSpecifyName;
                                      }
                                      return null;
                                    }),
                                const SizedBox(height: 24),
                                Text(
                                  AppLocalizations.of(context).pronoun,
                                  style:
                                      Theme.of(context).textTheme.labelMedium,
                                ),
                                Wrap(
                                    children: Pronoun.values.map((p) {
                                  return SmallRadio(
                                      padding:
                                          const EdgeInsetsDirectional.fromSTEB(
                                              0, 8, 12, 8),
                                      value: p,
                                      groupValue: pronoun,
                                      onChanged: (value) {
                                        if (value != null) {
                                          setState(() {
                                            pronoun = value;
                                          });
                                        }
                                      },
                                      child: Text(p.getTranslation(context)));
                                }).toList()),
                                const SizedBox(height: 12),
                                ElevatedButton(
                                    onPressed: () async {
                                      if (_formKey.currentState!.validate()) {
                                        _setPersonAndClose(TemporaryPerson(
                                            name: _textEditingController.text
                                                .trim(),
                                            pronoun: pronoun));
                                      }
                                    },
                                    child: Text(AppLocalizations.of(context)
                                        .addNewPerson))
                              ]
                            ],
                          ));
                    } else if (snapshot.hasError) {
                      return const Center(child: ErrorContent());
                    } else {
                      return const SizedBox();
                    }
                  } else {
                    return const Center(
                        child: CircularProgressIndicator.adaptive());
                  }
                })));
  }

  void _setPersonAndClose(TemporaryPerson person) {
    ref.read(situationProvider.notifier).addPerspective(person);
    ref.read(journalViewStateProvider.notifier).setCurrentPerson(person);

    Navigator.of(context).pop();
  }
}
