// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/models/temporary_person.dart';
import 'package:empado/providers/situation_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ObservationDialog extends ConsumerStatefulWidget {
  // TODO add more info on what constitutes non-judgmental observations
  final TemporaryPerson person;

  const ObservationDialog({required this.person, super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _ObservationDialogState();
}

class _ObservationDialogState extends ConsumerState<ObservationDialog> {
  final _formKey = GlobalKey<FormState>();
  late final TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController(
        text: ref
            .read(situationProvider)
            .state
            .perspectives[widget.person]!
            .observation);
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final situationNotifier = ref.read(situationProvider.notifier);

    return Dialog.fullscreen(
        child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  tooltip: AppLocalizations.of(context).discardChanges,
                  icon: const Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  }), //TODO warn about losing changes
            ),
            floatingActionButton: FloatingActionButton.extended(
              icon: const Icon(Icons.done),
              label: Text(AppLocalizations.of(context).done),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  situationNotifier.setObservation(
                      widget.person, _textEditingController.text.trim());
                  Navigator.of(context).pop();
                }
              },
            ),
            body: SingleChildScrollView(
              padding: const EdgeInsets.fromLTRB(16, 0, 16, 56 + 16 * 2),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                      //TODO add hero
                      child: SvgPicture.asset("assets/context_observation.svg",
                          width: 128)),
                  const SizedBox(height: 12),
                  Container(
                      alignment: Alignment.center,
                      child: Text(
                          AppLocalizations.of(context).relevantObservation,
                          style: Theme.of(context).textTheme.titleLarge,
                          textAlign: TextAlign.center)),
                  const SizedBox(height: 12),
                  Text(
                      AppLocalizations.of(context)
                          .relevantObservationLongerDesc,
                      style: Theme.of(context).textTheme.bodyMedium,
                      textAlign: TextAlign.left),
                  const SizedBox(height: 16),
                  Form(
                      key: _formKey,
                      child: TextFormField(
                        autofocus: true,
                        controller: _textEditingController,
                        minLines: 2,
                        maxLines: null,
                      )),
                ],
              ),
            )));
  }
}
