// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/enums/need.dart';
import 'package:empado/models/temporary_person.dart';
import 'package:empado/providers/situation_provider.dart';
import 'package:empado/utils/string_util.dart';
import 'package:empado/utils/theme_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';

class StrategyDialog extends ConsumerStatefulWidget {
  final TemporaryPerson person;

  const StrategyDialog({required this.person, super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _StrategyDialogState();
}

class _StrategyDialogState extends ConsumerState<StrategyDialog> {
  static const _replacementPlaceholder = "@@";
  final _formKey = GlobalKey<FormState>();
  late final TextEditingController _textEditingController;

  //TODO add info that can't rely on actions on behalf of others; what do they have power over / what can they influence themselves?

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController(
        text: ref
            .read(situationProvider)
            .state
            .perspectives[widget.person]!
            .strategies
            .join("\n"));
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final situationNotifier = ref.read(situationProvider.notifier);
    final perspectives = ref.read(situationProvider).state.perspectives;
    final perspective = perspectives[widget.person]!;

    final textStyle = Theme.of(context).textTheme.bodyMedium;

    final mainQuestionRaw = widget.person.isYou()
        ? perspective.needs.isEmpty
            ? perspective.isAppreciation
                ? AppLocalizations.of(context).whatAreSomeWaysYouMeetingEmpty
                : AppLocalizations.of(context).whatAreSomeWaysYouEmpty
            : perspective.isAppreciation
                ? AppLocalizations.of(context)
                    .whatAreSomeWaysYouMeeting(_replacementPlaceholder)
                : AppLocalizations.of(context)
                    .whatAreSomeWaysYou(_replacementPlaceholder)
        : perspective.needs.isEmpty
            ? perspective.isAppreciation
                ? AppLocalizations.of(context).whatAreSomeWaysTheyMeetingEmpty(
                    widget.person.name!, widget.person.pronoun.localeKey)
                : AppLocalizations.of(context).whatAreSomeWaysTheyEmpty(
                    widget.person.name!, widget.person.pronoun.localeKey)
            : perspective.isAppreciation
                ? AppLocalizations.of(context).whatAreSomeWaysTheyMeeting(
                    widget.person.name!,
                    _replacementPlaceholder,
                    widget.person.pronoun.localeKey)
                : AppLocalizations.of(context).whatAreSomeWaysThey(
                    widget.person.name!,
                    _replacementPlaceholder,
                    widget.person.pronoun.localeKey);
    final mainQuestionRawSplit = mainQuestionRaw.split(_replacementPlaceholder);

    final List<String> othersNeedsBullets = List.empty(growable: true);
    for (final p in perspectives.values) {
      if (p.person != widget.person && p.needs.isNotEmpty) {
        othersNeedsBullets.add(StringUtil.bullet +
            AppLocalizations.of(context).personIntro(
                p.person.name ?? AppLocalizations.of(context).you) +
            _getNeedsString(p.needs)!);
      }
    }

    return Dialog.fullscreen(
        child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  tooltip: AppLocalizations.of(context).discardChanges,
                  icon: const Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  }), //TODO warn about losing changes
            ),
            floatingActionButton: FloatingActionButton.extended(
              icon: const Icon(Icons.done),
              label: Text(AppLocalizations.of(context).done),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  situationNotifier.setStrategies(
                      widget.person, _textEditingController.text);
                  Navigator.of(context).pop();
                }
              },
            ),
            body: SingleChildScrollView(
              padding: const EdgeInsets.fromLTRB(16, 0, 16, 56 + 16 * 2),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                      child: SvgPicture.asset("assets/context_strategies.svg",
                          width: 128)),
                  const SizedBox(height: 12),
                  Container(
                      alignment: Alignment.center,
                      child: Text(
                          perspective.isAppreciation
                              ? AppLocalizations.of(context).strategiesUsed
                              : AppLocalizations.of(context)
                                  .potentialStrategies,
                          style: Theme.of(context).textTheme.titleLarge,
                          textAlign: TextAlign.center)),
                  const SizedBox(height: 8),
                  if (perspective.needs.isNotEmpty)
                    RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(children: [
                          TextSpan(
                              text: mainQuestionRawSplit.first,
                              style: textStyle),
                          TextSpan(
                              text: _getNeedsString(perspective.needs),
                              style: textStyle?.copyWith(
                                  fontVariations: ThemeUtil.boldVariation)),
                          TextSpan(
                              text: mainQuestionRawSplit.last,
                              style: textStyle),
                        ]))
                  else
                    Text(
                      mainQuestionRaw,
                      style: textStyle,
                    ),
                  if (othersNeedsBullets.isNotEmpty &&
                      !perspective.isAppreciation)
                    Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Text(
                            AppLocalizations.of(context).meetOthersNeedsToo,
                            style: textStyle)),
                  if (othersNeedsBullets.isNotEmpty &&
                      !perspective.isAppreciation)
                    ...othersNeedsBullets.map((s) => Text(s, style: textStyle)),
                  const SizedBox(height: 8),
                  Text(AppLocalizations.of(context).writeOneStrategyPerLine,
                      style: textStyle?.copyWith(
                          fontVariations: ThemeUtil.boldVariation)),
                  const SizedBox(height: 16),
                  Form(
                      key: _formKey,
                      child: TextFormField(
                        //TODO replace with a list of text fields, where navigating with arrows or backspace from the start of a field would move the cursor to the previous text field
                        // See https://github.com/yakupbaser/pintextfield/blob/main/lib/pintextfield.dart for inspiration
                        autofocus: true,
                        controller: _textEditingController,
                        minLines: 2,
                        maxLines: null,
                      ))
                ],
              ),
            )));
  }

  String? _getNeedsString(Set<Need>? needs) => needs
      ?.map((n) => n.getTranslation(context))
      .join(AppLocalizations.of(context).listJoiner);
}
