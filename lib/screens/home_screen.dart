import 'package:empado/enums/nav_item.dart';
import 'package:empado/screens/history/history_subscreen.dart';
import 'package:empado/screens/journal/journal_subscreen.dart';
import 'package:empado/screens/settings/setting_subscreen.dart';
import 'package:empado/screens/strategies/strategy_subscreen.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  NavItem currentNavItem = NavItem.journal;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: switch (currentNavItem) {
        NavItem.journal => const JournalSubscreen(),
        NavItem.strategies => const StrategySubscreen(),
        NavItem.history => const HistorySubscreen(),
        NavItem.settings => const SettingSubscreen(),
      },
      bottomNavigationBar: NavigationBar(
        selectedIndex: currentNavItem.index,
        destinations: NavItem.values
            .map((ni) => NavigationDestination(
                icon: Icon(ni.icon), label: ni.getTranslation(context)))
            .toList(),
        onDestinationSelected: (index) {
          setState(() {
            currentNavItem = NavItem.values[index];
          });
        },
      ),
    );
  }
}
