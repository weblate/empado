// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/components/perspective_statement/history_perspective_statement.dart';
import 'package:empado/components/perspective_statement/perspective_column.dart';
import 'package:empado/models/list_strategy.dart';
import 'package:empado/providers/db_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class StrategySheet extends ConsumerWidget {
  final ListStrategy listStrategy;

  const StrategySheet({required this.listStrategy, super.key});

  static Future<void> showSheet(
    BuildContext context, {
    required ListStrategy listStrategy,
  }) async {
    await showModalBottomSheet<void>(
        context: context,
        isScrollControlled: true,
        isDismissible: true,
        useSafeArea: true,
        builder: (_) => StrategySheet(listStrategy: listStrategy));
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DraggableScrollableSheet(
        expand: false,
        initialChildSize: 0.75,
        snap: true,
        snapSizes: const [0.75, 1.0],
        builder: (context, scrollController) {
          return CustomScrollView(
            controller: scrollController,
            slivers: [
              SliverAppBar(
                backgroundColor: Colors.transparent,
                title: Text(listStrategy.strategy.strategy,
                    style: Theme.of(context).textTheme.titleLarge),
                toolbarHeight: 56,
                leading: IconButton(
                    tooltip:
                        AppLocalizations.of(context).closeSituationOverview,
                    onPressed: () => Navigator.of(context).pop(),
                    icon: const Icon(Icons.expand_more)),
              ),
              SliverToBoxAdapter(
                  child: FutureBuilder(
                      future: ref
                          .read(dbProvider)
                          .queryStrategyPerspectives(listStrategy.strategy.id),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          final sps = snapshot.data!;
                          if (sps.isEmpty) {
                            return Center(
                                child: Text(AppLocalizations.of(context)
                                    .strategyNotUsedYet));
                          } else {
                            return Column(
                                mainAxisSize: MainAxisSize.min,
                                children: sps
                                    .map((sp) => Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16),
                                        child: Card(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .surfaceVariant,
                                            child: Padding(
                                                padding: const EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 24),
                                                child: PerspectiveColumn(
                                                    isAppreciation: sp
                                                        .perspective
                                                        .isAppreciation,
                                                    perspectiveStatement:
                                                        HistoryPerspectiveStatement(
                                                            listPerspective:
                                                                sp),
                                                    observation: sp.perspective
                                                        .observation,
                                                    strategies: const [],
                                                    additionalNotes:
                                                        sp.perspective.additionalNotes)))))
                                    .toList());
                          }
                        } else {
                          return const Center(
                              child: CircularProgressIndicator.adaptive());
                        }
                      }))
            ],
          );
        });
  }
}
