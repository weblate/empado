// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'temporary_person.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TemporaryPerson {
  int? get id => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;

  /// name == null => person is you
  Pronoun get pronoun => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TemporaryPersonCopyWith<TemporaryPerson> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TemporaryPersonCopyWith<$Res> {
  factory $TemporaryPersonCopyWith(
          TemporaryPerson value, $Res Function(TemporaryPerson) then) =
      _$TemporaryPersonCopyWithImpl<$Res, TemporaryPerson>;
  @useResult
  $Res call({int? id, String? name, Pronoun pronoun});
}

/// @nodoc
class _$TemporaryPersonCopyWithImpl<$Res, $Val extends TemporaryPerson>
    implements $TemporaryPersonCopyWith<$Res> {
  _$TemporaryPersonCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? pronoun = null,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      pronoun: null == pronoun
          ? _value.pronoun
          : pronoun // ignore: cast_nullable_to_non_nullable
              as Pronoun,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TemporaryPersonImplCopyWith<$Res>
    implements $TemporaryPersonCopyWith<$Res> {
  factory _$$TemporaryPersonImplCopyWith(_$TemporaryPersonImpl value,
          $Res Function(_$TemporaryPersonImpl) then) =
      __$$TemporaryPersonImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int? id, String? name, Pronoun pronoun});
}

/// @nodoc
class __$$TemporaryPersonImplCopyWithImpl<$Res>
    extends _$TemporaryPersonCopyWithImpl<$Res, _$TemporaryPersonImpl>
    implements _$$TemporaryPersonImplCopyWith<$Res> {
  __$$TemporaryPersonImplCopyWithImpl(
      _$TemporaryPersonImpl _value, $Res Function(_$TemporaryPersonImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? pronoun = null,
  }) {
    return _then(_$TemporaryPersonImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      pronoun: null == pronoun
          ? _value.pronoun
          : pronoun // ignore: cast_nullable_to_non_nullable
              as Pronoun,
    ));
  }
}

/// @nodoc

class _$TemporaryPersonImpl extends _TemporaryPerson {
  const _$TemporaryPersonImpl(
      {this.id = null, this.name, this.pronoun = Pronoun.they})
      : super._();

  @override
  @JsonKey()
  final int? id;
  @override
  final String? name;

  /// name == null => person is you
  @override
  @JsonKey()
  final Pronoun pronoun;

  @override
  String toString() {
    return 'TemporaryPerson(id: $id, name: $name, pronoun: $pronoun)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TemporaryPersonImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.pronoun, pronoun) || other.pronoun == pronoun));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, name, pronoun);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TemporaryPersonImplCopyWith<_$TemporaryPersonImpl> get copyWith =>
      __$$TemporaryPersonImplCopyWithImpl<_$TemporaryPersonImpl>(
          this, _$identity);
}

abstract class _TemporaryPerson extends TemporaryPerson {
  const factory _TemporaryPerson(
      {final int? id,
      final String? name,
      final Pronoun pronoun}) = _$TemporaryPersonImpl;
  const _TemporaryPerson._() : super._();

  @override
  int? get id;
  @override
  String? get name;
  @override

  /// name == null => person is you
  Pronoun get pronoun;
  @override
  @JsonKey(ignore: true)
  _$$TemporaryPersonImplCopyWith<_$TemporaryPersonImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
