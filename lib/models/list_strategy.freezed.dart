// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'list_strategy.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ListStrategy {
  Strategy get strategy => throw _privateConstructorUsedError;
  List<StrategyNeed> get strategyNeeds => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ListStrategyCopyWith<ListStrategy> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ListStrategyCopyWith<$Res> {
  factory $ListStrategyCopyWith(
          ListStrategy value, $Res Function(ListStrategy) then) =
      _$ListStrategyCopyWithImpl<$Res, ListStrategy>;
  @useResult
  $Res call({Strategy strategy, List<StrategyNeed> strategyNeeds});
}

/// @nodoc
class _$ListStrategyCopyWithImpl<$Res, $Val extends ListStrategy>
    implements $ListStrategyCopyWith<$Res> {
  _$ListStrategyCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? strategy = freezed,
    Object? strategyNeeds = null,
  }) {
    return _then(_value.copyWith(
      strategy: freezed == strategy
          ? _value.strategy
          : strategy // ignore: cast_nullable_to_non_nullable
              as Strategy,
      strategyNeeds: null == strategyNeeds
          ? _value.strategyNeeds
          : strategyNeeds // ignore: cast_nullable_to_non_nullable
              as List<StrategyNeed>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ListStrategyImplCopyWith<$Res>
    implements $ListStrategyCopyWith<$Res> {
  factory _$$ListStrategyImplCopyWith(
          _$ListStrategyImpl value, $Res Function(_$ListStrategyImpl) then) =
      __$$ListStrategyImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Strategy strategy, List<StrategyNeed> strategyNeeds});
}

/// @nodoc
class __$$ListStrategyImplCopyWithImpl<$Res>
    extends _$ListStrategyCopyWithImpl<$Res, _$ListStrategyImpl>
    implements _$$ListStrategyImplCopyWith<$Res> {
  __$$ListStrategyImplCopyWithImpl(
      _$ListStrategyImpl _value, $Res Function(_$ListStrategyImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? strategy = freezed,
    Object? strategyNeeds = null,
  }) {
    return _then(_$ListStrategyImpl(
      strategy: freezed == strategy
          ? _value.strategy
          : strategy // ignore: cast_nullable_to_non_nullable
              as Strategy,
      strategyNeeds: null == strategyNeeds
          ? _value._strategyNeeds
          : strategyNeeds // ignore: cast_nullable_to_non_nullable
              as List<StrategyNeed>,
    ));
  }
}

/// @nodoc

class _$ListStrategyImpl implements _ListStrategy {
  const _$ListStrategyImpl(
      {required this.strategy, required final List<StrategyNeed> strategyNeeds})
      : _strategyNeeds = strategyNeeds;

  @override
  final Strategy strategy;
  final List<StrategyNeed> _strategyNeeds;
  @override
  List<StrategyNeed> get strategyNeeds {
    if (_strategyNeeds is EqualUnmodifiableListView) return _strategyNeeds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_strategyNeeds);
  }

  @override
  String toString() {
    return 'ListStrategy(strategy: $strategy, strategyNeeds: $strategyNeeds)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ListStrategyImpl &&
            const DeepCollectionEquality().equals(other.strategy, strategy) &&
            const DeepCollectionEquality()
                .equals(other._strategyNeeds, _strategyNeeds));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(strategy),
      const DeepCollectionEquality().hash(_strategyNeeds));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ListStrategyImplCopyWith<_$ListStrategyImpl> get copyWith =>
      __$$ListStrategyImplCopyWithImpl<_$ListStrategyImpl>(this, _$identity);
}

abstract class _ListStrategy implements ListStrategy {
  const factory _ListStrategy(
      {required final Strategy strategy,
      required final List<StrategyNeed> strategyNeeds}) = _$ListStrategyImpl;

  @override
  Strategy get strategy;
  @override
  List<StrategyNeed> get strategyNeeds;
  @override
  @JsonKey(ignore: true)
  _$$ListStrategyImplCopyWith<_$ListStrategyImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
