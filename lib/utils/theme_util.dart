// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThemeUtil {
  static const boldVariation = <FontVariation>[FontVariation('wght', 700)];
  static const _semiboldVariation = <FontVariation>[FontVariation('wght', 550)];
  static const _normalVariation = <FontVariation>[FontVariation('wght', 400)];

  static ColorScheme lightScheme = ColorScheme.fromSeed(
      brightness: Brightness.light,
      seedColor: const Color(0xFFE57C00),
      primary: const Color(0xFFA55018));

  static ColorScheme darkScheme = ColorScheme.fromSeed(
      brightness: Brightness.dark,
      seedColor: const Color(0xFF543A57),
      primary: const Color(0xFFE57C00));

  static ThemeData getThemeFromScheme(ColorScheme colorScheme) {
    return ThemeData(
        useMaterial3: true,
        cupertinoOverrideTheme: const CupertinoThemeData(applyThemeToAll: true),
        fontFamily: "SourceSans",
        colorScheme: colorScheme,
        dividerColor: colorScheme.primary, //this is for ExpansionTile
        inputDecorationTheme: const InputDecorationTheme(
          border: OutlineInputBorder(),
        ),
        pageTransitionsTheme: const PageTransitionsTheme(
            builders: <TargetPlatform, PageTransitionsBuilder>{
              TargetPlatform.android: ZoomPageTransitionsBuilder(),
              TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
              TargetPlatform.linux: CupertinoPageTransitionsBuilder(),
              TargetPlatform.macOS: CupertinoPageTransitionsBuilder()
            }),
        appBarTheme: AppBarTheme(
            elevation: 0,
            scrolledUnderElevation: 4,
            shadowColor: colorScheme.shadow,
            centerTitle: true,
            surfaceTintColor: Colors.transparent),
        navigationBarTheme: NavigationBarThemeData(
            height: 56,
            labelBehavior: NavigationDestinationLabelBehavior.alwaysHide,
            backgroundColor: ElevationOverlay.colorWithOverlay(
                colorScheme.secondaryContainer, colorScheme.surfaceTint, 8.0),
            // backgroundColor: ElevationOverlay.colorWithOverlay(
            //     colorScheme.surface, colorScheme.surfaceTint, 16.0),
            // backgroundColor: ElevationOverlay.colorWithOverlay(
            // colorScheme.surfaceVariant, colorScheme.secondary, 8.0),
            surfaceTintColor: Colors.transparent,
            indicatorColor: colorScheme.secondary,
            iconTheme: MaterialStateProperty.resolveWith<IconThemeData>(
                (states) => (!states.contains(MaterialState.disabled) &&
                        states.contains(MaterialState.selected))
                    ? IconThemeData(color: colorScheme.onSecondary)
                    : IconThemeData(color: colorScheme.onSecondaryContainer))
            // backgroundColor: colorScheme.inverseSurface,
            // indicatorColor: colorScheme.onInverseSurface,
            ),
        textTheme: const TextTheme(
          displayLarge: TextStyle(fontVariations: _semiboldVariation),
          displayMedium: TextStyle(fontVariations: _semiboldVariation),
          displaySmall: TextStyle(fontVariations: _semiboldVariation),
          headlineLarge: TextStyle(fontVariations: _semiboldVariation),
          headlineMedium: TextStyle(fontVariations: _semiboldVariation),
          headlineSmall: TextStyle(fontVariations: _semiboldVariation),
          titleLarge: TextStyle(fontVariations: _semiboldVariation),
          titleMedium: TextStyle(fontVariations: _semiboldVariation),
          titleSmall: TextStyle(fontVariations: _semiboldVariation),
          bodyLarge: TextStyle(fontVariations: _normalVariation),
          bodyMedium: TextStyle(fontVariations: _normalVariation),
          bodySmall: TextStyle(fontVariations: _normalVariation),
          labelLarge: TextStyle(fontVariations: _semiboldVariation),
          labelMedium: TextStyle(fontVariations: _semiboldVariation),
          labelSmall: TextStyle(fontVariations: _semiboldVariation),
        ));
  }
}
