// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/utils/string_util.dart';
import 'package:empado/utils/theme_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PerspectiveColumn extends StatelessWidget {
  //TODO make text selectable
  final Widget perspectiveStatement;
  final bool isAppreciation;
  final String? observation;
  final Iterable<String> strategies;
  final String? additionalNotes;

  const PerspectiveColumn(
      {required this.isAppreciation,
      required this.perspectiveStatement,
      required this.observation,
      required this.strategies,
      required this.additionalNotes,
      super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // SizedBox(height: _verticalPadding),
          if (observation != null) Text(observation!),
          perspectiveStatement,
          if (strategies.isNotEmpty)
            Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 4),
                child: Text(
                    isAppreciation
                        ? AppLocalizations.of(context).strategiesUsed
                        : AppLocalizations.of(context).potentialStrategies,
                    style: const TextStyle(
                        fontVariations: ThemeUtil.boldVariation))),
          if (strategies.isNotEmpty)
            Text(StringUtil.toBulletedList(strategies)),
          if (additionalNotes != null)
            Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 4),
                child: Text(AppLocalizations.of(context).additionalNotes,
                    style: const TextStyle(
                        fontVariations: ThemeUtil.boldVariation))),
          if (additionalNotes != null) Text(additionalNotes!),
        ]);
  }
}
