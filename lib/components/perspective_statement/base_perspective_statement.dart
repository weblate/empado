// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/enums/pronoun.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BasePerspectiveStatement extends StatelessWidget {
  static const _cuttingSymbol = "@";
  static const _placeholderStartSymbol = "{";
  static const _placeholderEndSymbol = "}";
  static const _placeholderEmotions =
      "${_placeholderStartSymbol}emotions$_placeholderEndSymbol";
  static const _placeholderNeedStatement =
      "${_placeholderStartSymbol}needStatement$_placeholderEndSymbol";
  static const _placeholderNeeds =
      "${_placeholderStartSymbol}needs$_placeholderEndSymbol";

  final String? personName;
  final Pronoun personPronoun;
  final bool isOwn;

  final Iterable<InlineSpan> emotionSpans;
  final InlineSpan needStatementSpan;
  final Iterable<InlineSpan> needSpans;
  final TextStyle? textStyle;

  const BasePerspectiveStatement(
      {required this.personName,
      required this.personPronoun,
      required this.isOwn,
      required this.emotionSpans,
      required this.needStatementSpan,
      required this.needSpans,
      required this.textStyle,
      super.key});

  @override
  Widget build(BuildContext context) {
    final sentence = personName == null
        ? AppLocalizations.of(context).perspectiveStatementI(
            _cuttingSymbol + _placeholderEmotions + _cuttingSymbol,
            _cuttingSymbol + _placeholderNeedStatement + _cuttingSymbol,
            _cuttingSymbol + _placeholderNeeds + _cuttingSymbol)
        : isOwn
            ? AppLocalizations.of(context).perspectiveStatementOwnThey(
                personName!,
                _cuttingSymbol + _placeholderEmotions + _cuttingSymbol,
                _cuttingSymbol + _placeholderNeedStatement + _cuttingSymbol,
                _cuttingSymbol + _placeholderNeeds + _cuttingSymbol,
                personPronoun.localeKey)
            : AppLocalizations.of(context).perspectiveStatementAssumedThey(
                personName!,
                _cuttingSymbol + _placeholderEmotions + _cuttingSymbol,
                _cuttingSymbol + _placeholderNeedStatement + _cuttingSymbol,
                _cuttingSymbol + _placeholderNeeds + _cuttingSymbol,
                personPronoun.localeKey);
    final sentenceParts = sentence.split(_cuttingSymbol);
    return RichText(
        text: TextSpan(
      style: textStyle,
      children: ((sentenceParts.fold<List<InlineSpan>>(
          List<InlineSpan>.empty(growable: true), (spansSoFar, str) {
        if (str.startsWith(_placeholderStartSymbol)) {
          switch (str) {
            case _placeholderEmotions:
              if (emotionSpans.isNotEmpty) {
                spansSoFar.addAll(emotionSpans);
              } else {
                spansSoFar.add(TextSpan(
                    text: AppLocalizations.of(context)
                        .perspectiveStatementEmotionPlaceholder,
                    style: textStyle));
              }
              return spansSoFar;
            case _placeholderNeedStatement:
              spansSoFar.add(needStatementSpan);
              return spansSoFar;
            case _placeholderNeeds:
              if (needSpans.isNotEmpty) {
                spansSoFar.addAll(needSpans);
              } else {
                spansSoFar.add(TextSpan(
                    text: AppLocalizations.of(context)
                        .perspectiveStatementNeedPlaceholder,
                    style: textStyle));
              }
              return spansSoFar;
            default:
              spansSoFar.add(TextSpan(text: str, style: textStyle));
              return spansSoFar;
          }
        } else {
          spansSoFar.add(TextSpan(text: str, style: textStyle));
          return spansSoFar;
        }
      }))),
    ));
  }
}
