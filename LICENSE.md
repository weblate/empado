Code
====
Code is licensed under the **GNU AGPL v3 or any later version with an app store section**, unless specified otherwise. See COPYING for its content.

As an additional permission under section 7, you are allowed to distribute the software through an app store, even if that store has restrictive terms and conditions that are incompatible with the AGPL, provided that the source is also available under the AGPL with or without this permission through a channel without those restrictive terms and conditions.

As a limitation under section 7, all unofficial builds and forks of the app must be clearly labeled as unofficial in the app's name (e.g. "Empado UNOFFICIAL", never just "Empado") or use a different name altogether. If any code changes are made, the fork should use a completely different name and app icon. All unofficial builds and forks MUST use a different application ID, in order to not conflict with a potential official release.

Images
======

**Emojitwo**

Provide a basis for emotion icons, tweaked for use in the project.

* Name: EmojiTwo
* Author: [EmojiTwo](https://emojitwo.github.io/), originally released as [Emojione 2.2](https://www.emojione.com/) by [Ranks.com](http://www.ranks.com/) with contributions from the Emojitwo community
* Source: https://github.com/EmojiTwo/emojitwo
* License: [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/legalcode)

**Noto emoji**

Used for illustrations throughout the app.

* Name: Noto Emoji
* Author: Google
* Source: https://github.com/googlefonts/noto-emoji
* License: [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html)

Fonts
=====

**Source Sans 3**

* Name: Source Sans 3
* Author: Paul D. Hunt, Adobe Fonts
* Source: https://github.com/adobe-fonts/source-sans
* License: [SIL OFL 1.1](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)