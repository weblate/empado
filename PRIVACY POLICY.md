# Empado Privacy Policy

Last updated: Oct. 5, 2023

Pertains to: the app "Empado - NVC journal" by Enjoying FOSS

All your data stays strictly on your device unless you deliberately and knowingly share it with someone — in which case it will be shared only with the people you specifically choose.

The app doesn't trasmit any of your personal information online whatsoever. No analytics, no tracking, nothing ad-related. There isn't even a server side to the app.